
STYLEDIR=style
SOURCE=source
OUTPUT=output

BIBFILE=$(SOURCE)/references.bib

pdf_tmp: 
	@mkdir output & \
	@pandoc  \
		--output "$(OUTPUT)/thesis.pdf" \
		--template="style/template.tex" \
		--include-in-header="style/preamble.tex" \
		--variable=fontsize:12pt \
		--variable=papersize:a4paper \
		--variable=documentclass:report \
		--pdf-engine=xelatex \
		$(SOURCE)/02_dichiarazione_originalita.md  \
		$(SOURCE)/03_abstract.md  \
		$(SOURCE)/04_ringraziamenti.md  \
		$(SOURCE)/05_indice.md  \
		$(SOURCE)/06_list_of_figures.md  \
		$(SOURCE)/07_list_of_tables.md  \
		$(SOURCE)/08_abbreviations.md  \
		$(SOURCE)/09_chapter_1.md  \
		$(SOURCE)/10_chapter_2.md  \
		$(SOURCE)/11_chapter_3.md  \
		$(SOURCE)/12_chapter_4.md  \
		$(SOURCE)/13_chapter_5.md  \
		$(SOURCE)/14_chapter_6.md  \
		$(SOURCE)/15_conclusion.md  \
		$(SOURCE)/16_appendix_1.md  \
		$(SOURCE)/17_appendix_2.md  \
		$(SOURCE)/18_bibliografia.md  \
		"$(SOURCE)/metadata.yml" \
		--filter=pandoc-xnos \
		--filter=pandoc-fignos \
		--filter=pandoc-tablenos \
		--filter=pandoc-eqnos \
		--filter pandoc-secnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl="$(STYLEDIR)/ref_format.csl" \
		--number-sections \
		--verbose

		
pdf: 
	@mkdir output & \
	pandoc  \
		--output "$(OUTPUT)/tesi_746572.pdf" \
		--template="style/template.tex" \
		--include-in-header="style/preamble.tex" \
		--variable=fontsize:12pt \
		--variable=papersize:a4paper \
		--variable=documentclass:report \
		--pdf-engine=xelatex \
		$(SOURCE)/02_dichiarazione_originalita.md \
		$(SOURCE)/03_abstract.md \
		$(SOURCE)/04_ringraziamenti.md \
		$(SOURCE)/05_indice.md \
		$(SOURCE)/06_list_of.md \
		$(SOURCE)/09_introduzione.md \
		$(SOURCE)/10_obiettivo_tesi.md \
		$(SOURCE)/11_communicazione.md \
		$(SOURCE)/12_materiale_metodi.md \
		$(SOURCE)/13_capitolo_5.md \
		$(SOURCE)/14_capitolo_6.md \
		$(SOURCE)/pie_di_pagina.md \
		$(SOURCE)/18_bibliografia.md \
		"$(SOURCE)/metadata.yml" \
		--filter=pandoc-xnos \
		--filter=pandoc-fignos \
		--filter=pandoc-tablenos \
		--filter=pandoc-eqnos \
		--filter=pandoc-secnos \
		--bibliography="$(BIBFILE)" \
		--citeproc \
		--csl="$(STYLEDIR)/informatics.csl" \
		--number-sections \
		--verbose 2>output.ou
		
		