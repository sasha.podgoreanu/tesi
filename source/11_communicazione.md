# Comunicazione del sistema informativo di laboratorio {#lis}

Per identificare i moduli software e i dispositivi che rallentano il middleware bisogna avere un'immagine completa di come funziona il TLA, i suoi principali componenti e i protocolli di comunicazione impiegati. 

## Protocolli di comunicazione

Un protocollo nella terminologia IT è un insieme di regole formali e ben definite, per lo scambio di informazioni tra diversi programmi per computer. Un vantaggio di utilizzare un middleware è anche quello di risolvere i problemi della comunicazione a causa dell'utilizzo di protocolli diversi da parte dalle aziende che producono procedure analitiche e dispositivi per la TLA.

### ASTM 1381 {#ASTM_1381}

ASTM 1381 o LIS1-A [@ASTM_E1381_low] è un protocollo di basso livello utilizzato per il trasporto di messaggi tra strumenti di laboratorio e sistemi informatici. La specifica descrive che il protocollo di basso livello viene utilizzato sia per lo scambio di dati binari seriali che per lo scambio di dati TCP/IP. Il middleware è composto da componenti distribuiti che sono configurati per accettare messaggi dagli strumenti di laboratorio. Il corpo di un messaggio può includere risultati delle analisi di un paziente, risultati dei controlli di qualità, informazioni sulla tracciabilità di una provetta e altre informazioni utili al middleware. 

Il livello di collegamento dati del protocollo interagisce con i livelli superiori di invio e ricezione di messaggi, gestisce la connessione dati e le richieste di apertura e rilascio della connessione della linea TCP/IP. Il modello di comunicazione è un modello basato su client-server in modalità sincrona. Il server può interagire solo con un client alla volta.

Nel blocco di testo \ref{lst:ASTM_1381} mostra lo scambio di messaggi che avviene tra un mittente e un destinatario utilizzando il protocollo ASTM 1381. Il mittente inizia la comunicazione inviando il primo carattere `<ENQ>`, il destinatario risponde con il carattere `<ACK>` se è pronto a ricevere i messaggi altrimenti risponde con il carattere `<NACK>` e la comunicazione viene interrotta. Una volta avviata la comunicazione, per ogni messaggio ricevuto correttamente, il destinatario risponde inviando `<ACK>`, altrimenti `<NACK>` se è necessaria una ritrasmissione dell’ultimo frame. Alla riga nove il frame della riga sette è stato ritrasmesso perché non ha superato il controllo della funzione hash. Il valore `DE` è la stringa di controllo del frame. Per segnalare la fine della trasmissione dei messaggi il mittente invia il carattere `<EOT>`.

\begin{lstlisting}[style=ASTM, caption=Record di esempio utilizzato dal protocollo ASTM 1381, label={lst:ASTM_1381}]
out: <ENQ> 
in : <ACK>
out: <STX>1|Messaggio 1<CR><ETX>59<CR><LF>
in : <ACK>
out: <STX>2|Messaggio 2<CR><ETX>1D<CR><LF>
in : <ACK>
out: <STX>3|Messaggio§3<CR><ETX>DE<CR><LF>
in : <NACK>
out: <STX>4|Messaggio 3<CR><ETX>DE<CR><LF>
in : <ACK>
out: <STX>5|Messaggio 4<CR><ETX>77<CR><LF>
in : <ACK>
out: <EOT>
\end{lstlisting}

### LIS2-A2 {#LIS2_A2}

LIS2 è una revisione del precedente ASTM E1381-02 [@ASTM_E1381_high]. Il protocollo consente a due sistemi qualsiasi di stabilire un link logico per la comunicazione del testo per l'invio di risultati, richieste o informazioni demografiche in una forma standardizzata e interpretabile. Lo scopo principale è di documentare le convenzioni comuni utili per lo scambio di risultati clinici e dati del paziente tra strumenti di laboratorio e sistemi informativi, il middleware. L'implementazione del protocollo LIS2 può ricevere in input messaggi completi dal protocollo ASTM 1381 sottostante.

In uno scenario comune l'automazione porta le provette al inlet[^inlet] degli strumenti analitici. L'analizzatore preleva una o più provette dal ingresso e scansiona il barcode presente sulle boccette. Il sistema informativo presente a bordo del dispositivo stabilisce una connessione con il sistema informativo del laboratorio e trasmette il messaggio \ref{lst:msg_richiesta_astm} per richiedere quali analisi deve eseguire sui campioni con il barcode 032989325 e 032989327. Il sistema informativo risponde con il messaggio \ref{lst:msg_risposta_astm}, dove ha individuato i campioni di due pazienti diversi, e per ciascun paziente le istruzioni con le analisi da eseguire. Lo strumento preleva materiale biologico tramite pipettaggio dalle provette, esegue la procedura d'analisi e al termine invia i risultati \ref{lst:msg_risultati_astm} al middleware.

\begin{lstlisting}[style=ASTM, caption=Messaggio contenente i record con la richiesta della programmazione dello strumento analitico per le provette 032989325 e 032989327, label={lst:msg_richiesta_astm}]
H|\^&|||Alinity hq^1.0.0.0^HQ00000001|||||||P|LIS2-A2|20170506160540-0700<CR>
    Q|1|^032989325\^032989327||ALL||||||||O<CR>
L|1|N<CR>
\end{lstlisting}

\begin{lstlisting}[style=ASTM, caption=Messaggio contenente i record della risposta dal sistema informativo con la programmazione della richiesta precedente, label={lst:msg_risposta_astm}]
H|\^&|||Middleware WireLab|||||||P|LIS2-A2|20170506160545-0700<CR>
    P|1|007686|123||Paziente^Uno||1949|M|||<CR>
        O|1|032989325|99910|^^^BUN|R|20221009||||R<CR>
        O|2|032989325|99912|^^^ISE|R|20221009||||R<CR>
    P|2|0086|||Paziente^Due||1987|F|||<CR>
        O|1|032989327|99913|^^^CHEM|R|20221009||||R<CR>
L|1|F|<CR>
\end{lstlisting}

\begin{lstlisting}[style=ASTM, caption=Messaggio contenente i record con i risultati inviati al sistema informativo dei test ordinati per le provette 032989325 e 032989327, label={lst:msg_risultati_astm}]
H|\^&|||Alinity hq^1.0.0.0^HQ00000001|||||||P|LIS2-A2|20170506154512-0700<CR>
	P|1||300001||PROVA^Uno||19350113|M|||||||||||||||||EST_VERSO<CR>
		O|1|032989325|032989325^1|^^^BUN+ISE|R||||||||||||||||F<CR>
			R|1|^^^BUN^F|2.94|10*3/miuL|0.00 - 999.|||F||||20170506151647|<CR>
			R|2|^^^ISE^F|1.54|30.7*3/miuL|0.00 - 999.|||F||||20170506151647|<CR>
				C|1|I|DC^NONE|I<CR>
	P|1||300001||PROVA^Uno||19350113|M|||||||||||||||||EST_VERSO<CR>
		O|1|032989327|032989327^1|^^^CHEM|R||||||||||||||F<CR>
			R|1|^^^CHEM^F|2|10*3/miuL|0.00 - 999.|||F||||20170506151647|<CR>
				C|1|I|DC^HIGH|I<CR>
L|1<CR>
\end{lstlisting}

### HL7

HL7 è un protocollo di comunicazione largamente utilizzato negli ospedali e nei laboratori, nonché nell'amministrazione [@hl7_wiki;@hl7_org]. Garantisce la comunicazione di sistemi informativi e lo scambio di dati eterogenei. Lo standard HL7 è un protocollo comunemente accettato da tutti i produttori di strumenti d'analisi in vitro. Utilizzando un modello del protocollo implementato, un analista del laboratorio può ricevere ordini di test direttamente dai reparti clinici e restituire in modo automatico i risultati degli esami svolti ai dipartimenti che hanno ordinato le analisi. Con l'aiuto di HL7 possono essere gestiti enormi volumi di informazioni mediche, di anagrafica e diagnostica. Lo standard HL7 può essere implementato con facilità sui sistemi informativi esistenti e nella attrezzatura medica già esistente. Non richiede modifiche e interconnette i sistemi e le macchine di qualsiasi produttore. 

Inoltre, è uno standard open source che consente l'interfacciamento tra applicazioni eterogenee, staccandosi dalla classica architettura client-server (es. \+@sec:ASTM_1381 e \+@sec:LIS2_A2) che presuppone la precisa definizione della relazione e di ruoli tra due parti. Il protocollo è orientato agli eventi, al verificarsi di un evento avviene uno scambio di messaggi HL7 con una serie di applicazioni interconnesse configurate per ricevere un certo tipo di informazione (es. ricovero di un paziente). 

WireLab, per comunicare con componenti ed entità nell’ambito Healthcare, utilizza entrambi i protocolli ASTM e HL7. Nella figura @fig:hl7_architetture, WireLab può essere presente in qualsiasi laboratorio e assumere il ruolo di front end per l'invio e la ricezione di messaggi. Per utilizzare il protocollo HL7 è sufficiente esporre un servizio REST HTTP/HTTPS e ricevere i messaggi. Sarà il componente che implementa il protocollo a smistare i messaggi verso il servizio che gestisce il messaggio specifico. Mentre la connessione del protocollo ASTM è sincrona inviando un messaggio alla volta, HL7 sfrutta la connessione asincrona dello strato HTTP e può gestire in parallelo invii e ricezioni di messaggi raggiungendo solo il limite fisico dell'infrastruttura.

![Connessioni asincrone del protocollo HL7 e connessioni sincrone del protocollo ASTM che collegano Il Laboratory Information Sistem e i laboratori di analisi in vitro nell'ambito Healthcare.](source/figures/hl7_architetture.png){#fig:hl7_architetture width=65%}

## Connessione

Il sistema informativo di laboratorio (LIS) è composto da componenti critici per le operazioni cliniche di un laboratorio nell’ambito ospedaliero [@10.5858/arpa.2012-0362-RA]. I LIS possono gestire flussi di lavoro di alto rendimento. In senso ampio, gestiscono il flusso di informazioni tra i sistemi di salute nazionali o privati e laboratori di analisi. I dati sono di natura amministrativa, diagnostica e anagrafica dei pazienti. Principalmente, il LIS invia al sistema informativo del laboratorio messaggi di accettazione, aggiornamenti e anagrafica utilizzando protocolli ASTM o HL7. I messaggi di accettazione sono messaggi che contengono informazioni sulla metodologia delle analisi da eseguire di una provetta. I messaggi di anagrafica e aggiornamenti della anagrafica vengono inviati quando un nuovo paziente è stato registrato preso una struttura ospedaliera o in un centro prelievi di materiale biologico. 

Gli strumenti di analisi in vitro e altri sistemi del laboratorio sono gestiti da un sistema di gestione dell'automazione, che pilota il sistema e che è collegato al LIS o ad un middleware [@CROXATTO2016217]. Il middleware è opzionale ed è necessario quando il LIS non supporta tutte le funzionalità, come la gestione dei protocolli analitici o il collegamento ad altri automatismi (Fig. @fig:connessione). Gli utenti interagiscono con il software di gestione dell'automazione per leggere i barcode delle provette, leggere ulteriori informazioni sui pazienti, validare i risultati e gestire il sistema di automazione del laboratorio. L'interazione avviene in modalità Web.

![L'utente interagisce con il sistema di gestione dell'automazione del laboratorio tramite un software di gestione direttamente connesso al LIS o indirettamente tramite un middleware. L'interazione dell'utente con il software di gestione dell'automazione viene eseguita o in modalità console o in modalità client-server (Web).](source/figures/connessione.png){#fig:connessione}

I laboratori che sono collegati direttamente a LIS non hanno bisogno di un middleware; quindi, questa configurazione non viene trattata in questo documento. Quando viene utilizzata una soluzione middleware, il LIS invia richieste analitiche al middleware, che implementa i protocolli analitici e pilota il sistema di gestione dell'automazione in tempo reale. In tale modalità, LIS invia solo i messaggi con le richieste analitiche al software del sistema di automazione di laboratorio, che gestisce le fasi delle procedure analitiche [@CROXATTO2016217].

Nei diversi laboratori, il middleware deve poter essere collegato a svariate configurazioni di sistemi di automazione che possono essere classificati in Total laboratory automation (TLA), automazione di laboratorio modulare e workcell/workstation automation [@Swaminathan22;@10.1093/clinchem/46.5.751;@10.1093/clinchem/48.3.540]. La tabella @tbl:1 elenca le alternative per l'automazione di un laboratorio, con esempi di alcuni produttori.

--------------------------------------------------------------------------------
Grado di automazione         Tipo di automazione               Azienda
---------------------------- -------------------------------- ------------------
Total laboratory automation  Diagnostics--independent system  Beckman Coulter
                                                              IDS
                                                              Labotix
                                                              Lab-Interlink

Total laboratory automation  Diagnostics associated           Roche
                                                              Bayer      

Partial automation           Diagnostics associated           Bayer--LabCell
                                                              Konelab Corporation

                             Independent systems              Tecan
                                                              Genesis
                                                              Olympus

                             Analytical modular systems       Bayer Advia IMS
                                                              Roche Modular
                                                              Abbott Architect 
                                                                
                             Postanalytical                   RTS Thurnall
--------------------------------------------------------------------------------

: Esempi delle diverse automazioni disponibili. {#tbl:1}

Una configurazione commune di un TLA viene mostrata nella figura @fig:tla_sample e i due laboratori analizzati in questo documento utilizzano una configurazione simile. All'automazione sono collegati dispositivi da diversi produttori che comunicano tramite i messaggi con il LabWare. Lo strumento PowerExpress è un'automazione che è composto da: l'inlet, la banda trasportatrice, l'outlet e il sistema informativo installato a bordo che comunica col middleware e con gli strumenti. Il sorter è uno strumento che raggruppa le provette in rack in base al colore del tappo. L'inlet dell'automazione accetta i rack con le provette e tramite un braccio meccanico li carica su una banda trasportatrice che vengono portate verso gli strumenti di laboratorio. Le provette di ematologia che hanno bisogno di essere centrifugati passano dalle centrifughe. Il decapper toglie il tappo delle provette, ACL Top è uno strumento di analisi di coagulazione, DxH 800 e Alinity hq sono strumenti analitici di ematologia, DxU sono strumenti di analisi delle urine e infine il recaper rimette il tappo alle provette. I contenitori che devono essere stoccati per ulteriori esami vengono inviati allo stockyard, la cella frigo composta da rack e un braccio meccanico. L'outlet è il modulo di uscita delle provette dall'automazione che ripone le provette in rack e li separa. In questo esempio, gli strumenti di ematologia ACL Top, DxH 800 e Alinity hq vengono forniti da tre produttori diversi.

![Una configurazione di un TLA](source/figures/tla_sample.png){#fig:tla_sample}

## Flussi di messaggi

Nella figura @fig:flussi_messaggi vengono mostrati i collegamenti fisici tra gli strumenti, sistema di gestione dell'automazione di laboratorio e il middleware [@hawker2018automation]. La linea di colore marrone rappresenta il link fisico e logico dove avvengono gli scambi di messaggi del laboratorio con il middleware. Le linee continue e le frecce rappresentano i flussi dei messaggi. La linea tratteggiata e le frecce mostrano i flussi informativi logici consentiti, ma non standardizzati. Tutti gli strumenti, con alcune eccezioni, sono collegati al sistema di gestitone dell'automazione del laboratorio che comunica con il middleware. Alcuni strumenti di analisi in vitro sono collegati direttamente al middleware e non alla automazione. Le prime indagini per la ricerca di una soluzione del problema presentato nella sezione @collo_di_bottiglia_TLA si concentrano in questo punto, ovvero il flusso di messaggi.

![Modello della connessione fisica.](source/figures/flussi_messaggi.png){#fig:flussi_messaggi}

I flussi di messaggi scambiati tra LIS, middleware e sistema di gestione dell'automazione di laboratorio sono classificati in tre fasi principali[@IoMUSCMPMCzd]:

- *Fase pre-analitica*: selezione dell'analisi, etichettatura della provetta, creazione dell’ordine, raccolta del campione[^3], trasporto del campione dal centro di prelievo verso il laboratorio di analisi. La creazione dell’ordine è particolarmente importante perché riguarda la creazione del record elettronico che contiene informazioni sul paziente e la sua anagrafica, eventuali patologie, il medico curante e le analisi per il quale il campione è stato raccolto.
- *Fase analitica*: include l'esecuzione dell'analisi sui campioni e l'esecuzione di una serie di procedure per produrre il risultato. 
- *Fase post-analitica*: riguarda principalmente l'invio di risultati prodotti dalle procedure analitiche[^4] verso l'ospedale o l'ente che ha richiesto l'analisi. 

### Fase pre-analitica {#fase_pre_analitica}

In questa fase diverse entità cliniche si scambiano messaggi di anagrafica, messaggi della registrazione di un paziente preso una struttura ospedaliera, messaggi contenenti informazioni sul prelievo di materiale biologico, etc. L'obbiettivo principale dello scambio di dati è la creazione dei record della anagrafica e delle accettazioni delle provette, che sono estrapolati dai messaggi inviati da parte del LIS. Il laboratorio, inoltre, crea la lista di lavoro, che è rappresentata nel database da una tabella che tiene traccia di tutti i test che devono essere eseguiti. Questa tabella è il cuore dell'applicazione, con migliaia di inserimenti, aggiornamenti e interrogazioni al minuto. I lock costanti su questa tabella possono essere una causa dei blocchi generali descritti nella sezione @collo_di_bottiglia_TLA. Nell’esempio \ref{lst:OML_O33} viene mostrato un messaggio HL7 di accettazione di una provetta. Il messaggio \ref{lst:MFN_M13} contiene la cancellazione di un Comune mentre il messaggio \ref{lst:MFN_M14} contiene un aggiornamento delle anagrafica di un paziente. I messaggi, principalmente, sono delle istruzioni per guidare il laboratorio su quali analisi eseguire, come eseguirle (aliquote) e che tipologia di reagenti preferibilmente utilizzare per l'esecuzione delle analisi (metodiche)[@hl7_definition].

\begin{lstlisting}[
  style=ASTM, 
  caption={Messaggio OML O33 (Accettazione provetta). Ogni riga del messaggio contiene un segmento con informazioni specifiche:\\
MSH - Intestazione del messaggio\\
PID - Identificativo paziente\\
PV1 - Identificazione del medico\\
SPM - Descrive le caratteristiche di un esemplare\\
SAC - Identificazione del contenitore del campione\\
ORC - Informazioni sul reparto che ordina le analisi\\
TQ1 - Informazioni sulle quantità e priorità della esecuzione del test\\
OBR - Informazioni specifiche dell'accettazione per uno studio diagnostico o un'osservazione, un esame fisico o una valutazione}, 
  label={lst:OML_O33}]
MSH|^~\&|OPEN_LIS|OPEN_LIS Spa|WireLab|Wire Group|20190530102424||OML^O33^OML_O33|164150|P|2.5
PID|1||~~~~000005^^^XMPI^PK||PROVA^CINQUE||19431202|F
PV1|1|U|SPA00005||||^DOTT^CINQUE||||||||||||9000000005|||||||||||||||||||||||||||||||||controllo
SPM|1|999999905||SIERO
SAC|||8888888805||||20190530103000||||||||2
ORC|NW|1000000005|||||||20190530102424||||||||||||REPARTO-CARDIOLOGIA
TQ1|5||||||||00
OBR|1|1000000005||EMOCROMO||||||||||||||EMOCROMO|EMOCROMO
ORC|NW|1000000005|||||||20190530102424||||||||||||REPARTO-PEDIATRIA
TQ1|1||||||||01
OBR|1|1000000005||Alfa-2-globuline||||||||||||||Alfa-2-globuline|Alfa-2-globuline
SPM|1|999990001||URINE
SAC|||8888888805||||20190530103000||||||||2
ORC|NW|1000000005|||||||20190530102424||||||||||||REPARTO-CARDIOLOGIA
TQ1|3||||||||02
OBR|1|1000000005||LITHIUM||||||||||||||LITHIUM|LITHIUM
\end{lstlisting}

\begin{lstlisting}[style=ASTM, caption=Messaggio MFN M13 (Cancellazione Comune), label={lst:MFN_M13}]
MSH|^~\&|OPEN_LIS|OPEN_LIS Spa|WireLab|Wire Group|20190909162303||MFN^M13^MFN_M13|20190909162303|P|2.5
MFI|COM||UPD|20190909||ER
MFE|MDL|549||000206^BELGIO|CE
\end{lstlisting}

\begin{lstlisting}[style=ASTM, caption=Messaggio MFN M14 (Aggiornamento utenti), label={lst:MFN_M14}]
MSH|^~\&|OPEN_LIS|OPEN_LIS Spa|WireLab|Wire Group|20190829162959||MFN^M14^MFN_M14|20190829162959|P|2.5
MFI|UTZ||UPD|20190829|20990827|ER
MFE|MUP|199||utente^COGNOME UTENTE MODIFICATO^1^ADMIN|CE
\end{lstlisting}

### Fase analitica

La fase analitica è stata al centro della maggior parte degli sviluppi tecnologici nella scienza di laboratorio clinico ed è tipicamente associata alla più bassa frequenza di errori nel laboratorio clinico e i minor ritardi nel calcolo del TAT [@10.5858/arpa.2012-0362-RA]. WireLab offre un’interfaccia per la gestione di campioni e della strumentazione analitica per l'elaborazione semplificata delle richieste analitiche, inclusa la possibilità di indirizzare i test allo strumento appropriato in base al carico di lavoro, richiamare campioni per test ripetuti, diluire campioni diretti, elaborare le richieste delle provette per test aggiuntivi e registrare i risultati dei test e i commenti appropriati. La componente dashboard di WireLab ha il compito di offrire il monitoraggio della situazione generale dello svolgimento dei test nel laboratorio, mostrando grafici a torta o a bare. I campioni incompleti che hanno superato un limite di tempo preimpostato dal loro ingresso nel sistema, vengono evidenziati, così che gli analisti del laboratorio possono prendere azioni immediati. 

Anche in questa fase, la componente principale è la *lista di lavoro*, cui il compito è di facilitare l'elaborazione in batch di test sia manuali che automatizzati e di tracciare gli ordini che non sono stati completati. Il middleware deve gestire le seguenti tipologie di messaggi: 

- *Messaggi di tracciamento* contengono le informazioni sulla tranciabilità della provetta all’interno del laboratorio.
- *Messaggi di interrogazione* sono i messaggi inviati dagli strumenti analitici per richiedere la programmazione di una provetta associata ad un barcode per recuperare tutti i test da eseguire (messaggio \ref{lst:msg_richiesta_astm}).
- *Messaggi ordine* contengono informazioni dei test da eseguire (messaggio \ref{lst:msg_risposta_astm}).
- *Messaggi risultato* inviati dagli strumenti di analisi e contengono i record relativi alla esecuzione di test, commenti, valori di riferimento, alarmi, anomalie e indicazioni su un'eventuale diagnosi (messaggio \ref{lst:msg_risultati_astm}).

Al ricevimento del *messaggio risultato*, WireLab esegue procedure di validazione dei risultati delle analisi e applica algoritmi che impatta sul dato finale. Il processo deve essere riproducibile in caso di errori o ripetizioni.

### Fase post-analitica

Riguarda principalmente l'invio di risultati prodotti dalle procedure analitiche verso l'ospedale o l'ente che ha richiesto l'analisi. Nello specifico, i messaggi vengono inviati al LIS di riferimento quando sono stati validati dal sistema automatico o da un operatore. Anche in questa fase la tabella *lista di lavoro* viene interrogata costantemente. Solo con l'invio dei risultati a LIS si conclude il ciclo della provetta e il suo tempo trascorso nel ciclo del'analisi può essere aggiornato. Altre funzionalità importanti che WireLab mette a disposizione in questa fase è la generazione di una varietà di report da utilizzare nella cura del paziente, inclusi report standard e definibili dall'utente organizzati per test, gruppo di test, data, intervallo di date, fornitore o gruppo di fornitori che effettuano l'ordine, clinica o specialità, cumulativo sequenziale o tabulato. Questi servizi vengono offerti dalla componente software WirePrinting. Queste operazioni hanno un peso non indifferente sul blocco del sistema in quanto deve essere letta una mole di dati significativa per produrre i vari report.