# Materiali, metodi e analisi {#mma}

## Test Around Time (TAT)

<!-- TAT = test around time-->
> Secondo Manor @Manor1999-um il TAT fornisce una fonte consolidata di dati di benchmarking utili al laboratorio nella definizione degli obiettivi.
> Un tempo medio di completamento minore di 60 minuti, del 90% dei campioni dalla registrazione fino all'invio dei risultati, è suggerito come obiettivo iniziale per un TAT accettabile. 

I medici considerano il TAT dal momento in cui il test viene ordinato fino alla produzione del referto con i risultati, mentre le analisi di laboratorio usano il check-in[^registrazione] del campione e la refertazione dei risultati come TAT [@10.1309/H0DY-6VTW-NB36-U3L6]. Ci sono molti fattori che esulano dalla giurisdizione del laboratorio che influenzano il TAT. Tali ritardi non analitici possono essere responsabili fino al 96% del TAT totale [@Manor1999-um]. All’inizio del tirocinio, sono stati raccolti dati dal laboratorio Labo Centrale e da un laboratorio di analisi in vitro presente all’interno di un ospedale. Chiameremo quest'ultimo laboratorio Labo L, che utilizza una soluzione middleware WireLab con l'automazione fornita da Beckman Coulter. Nel Labo Centrale sono state eseguite ricerche, analisi di dati, analisi delle procedure analitiche del laboratorio, monitoraggio della rete interna e altri parametri.

Il laboratorio Labo L processa approssimativamente 2000 campioni al giorno e i blocchi del sistema informativo accadono di rado, le routine procedono ad una velocità costante e il TAT è considerato accettabile. Le provette arrivano dai reparti di cardiologia, chirurgia cardiotoracica, neurologia, neurochirurgia, gastroenterologia, gastrochirurgia e psichiatria. Al laboratorio arrivano provette anche dal reparto *fuori paziente dell’ospedale*, dipartimento OPD. Il personale registra la data e l'ora quando il materiale biologico è stato prelevato dal paziente. Questo dato segna un punto del conteggio del TAT sulla linea temporale. Le provette vengono trasportate al laboratorio e una volta arrivate sono caricate nel sorter che li inserisce nei rack. I rack vengono trasportati al inlet dell'automazione che legge il barcode di ogni provetta e invia i messaggi di check-in al middleware WireLab. L'evento di check-in segnala che la provetta associata al barcode letto è stata accettata ed è presente nel laboratorio.

Le tabelle @tbl:tat_labo_l e @tbl:tat_labo_centrale mostrano la distribuzione dei tempi di risposta per i diversi parametri osservati nel periodo di un mese dei due laboratori. Nel laboratorio Labo L, per la categoria reparti, il tempo medio che a partire dal prelievo del materiale biologico e fino alla refertazione dei risultati è di cinque ore, di quale solo un ora e mezza è dovuto ai ritardi intralaboratorio. Mentre nel laboratorio Labo Centrale, il valore del TAT quasi viene raddoppiato. Per il dipartimento OPD, il TAT di Labo L è di 24 ore perché i risultati vengono inviati il secondo giorno mentre nel Labo Centrale è di 7 ore, ma la permanenza nel laboratorio di analisi è maggiore di un'ora. La categoria più importante in un ospedale sono le provette nello stato di urgenza. Mentre Labo L riesce a produrre il referto in solo un'ora e mezza il Labo Centrale lo produce in tre ore. Si nota subito una differenza netta tra i due laboratori. Il TAT del laboratorio Labo L viene scelto come riferimento per il TAT di Labo Centrale che è da migliorare.

---------------------------------------------------------------------------------------------
Categoria   Total\  TAT\               Contributo della fase\      Contributo della fase\ 
            TAT     intralaboratorio   analitica                    pre e post analitica
----------- ------ ------------------ ------------------------ ------------------------------- 
Reparti      5 h    1–1.5 h                   30%                   70%

OPD         24 h    2.5–3 h                   15%                   85%

Urgenze     1.5 h    45 min                   50%                   50%
---------------------------------------------------------------------------------------------

: Contributo delle varie fasi di elaborazione del campione verso il TAT finale del laboratorio Labo L. Valori approssimativi. {#tbl:tat_labo_l}

---------------------------------------------------------------------------------------------
Categoria   Total\  TAT\               Contributo della fase\      Contributo della fase\ 
            TAT     intralaboratorio   analitica                    pre e post analitica
----------- ------ ------------------ ------------------------ ------------------------------- 
Reparti     6 h      2–2.5 h                40%                     60%

OPD         7 h      3.5–4 h                55%                     45%

Urgenze     3 h      2.5 h                  80%                     20%
---------------------------------------------------------------------------------------------

: Contributo delle varie fasi di elaborazione del campione verso il TAT finale del laboratorio Labo Centrale. Valori approssimativi. {#tbl:tat_labo_centrale}

Dalle ricerche svolte sono state scartate le possibilità che le procedure analitiche potrebbero essere insufficienti o non ottimizzate, i rallentamenti della rete o i rallentamenti causati dal flusso di lavoro svolto dagli operatori di laboratorio. Nel Labo Centrale, quando il sistema rallenta, i dispositivi di analisi aspettano ulteriori provette al inlet oppure la programmazione dal middleware per poter eseguire le analisi. La rete era utilizzata al 5% della capacità massima, la CPU veniva utilizzata al 15 % con picchi che non superavano il 50%, invece la RAM utilizzata dall'istanza della macchina dove è installato il database SQL Server era al 100%.

Dal database del middleware sono stati estratti i dati relativi al TAT delle provette nello stato di urgenza. I campioni con il check-in tra le ore 11 e ore 15 superavano oltre le due ore e mezza di permanenza nel laboratorio Labo Centrale prima che venisse generato il referto. Il TAT maggiore di 2.5 ore delle provette coincide con il periodo temporale in cui avvengono i blocchi. Nella figura @fig:message_burst_hour, viene mostrato il numero di campioni che arrivano al Labo Centrale e il numero di campioni gestiti raggruppati per ora. L'intera automazione tra le ore 11 e 14 rallenta significativamente il suo lavoro. Alle ore 13 il flusso di provette in entrata diminuisce ma il sistema continua a lavorare in modo non ottimale. Nella figura @fig:ram_used viene mostrata la percentuale della RAM utilizzata dall'istanza di SQL Server. I picchi del 100% della RAM coincidono con le ore dove l'automazione rallenta, ed è questo che ci porta a pensare che l'istanza del SQL Server può essere il collo di bottiglia del middleware. 

![Numero di campioni per ora accettati da Labo Centrale.](source/figures/message_burst_hour.png){#fig:message_burst_hour width=65%}

![Percentuale della RAM per ora utilizzata dall'istanza di SQL Server.](source/figures/ram_used.png){#fig:ram_used width=65%}

## SQL Server, collo di bottiglia del middleware o viceversa? {#sql_server_analisi}

SQL Server è installato su una macchina virtuale che gira su un server fisico. Si è deciso di aumentare la RAM fino a quadruplicare il limite iniziale e raggiungendo il limite massimo disponibile della macchina fisica. Indipendentemente dalla quantità di memoria che veniva aumentata, SQL Server utilizzava tutto ciò che poteva ottenere e memorizzava nella cache intere o porzioni di tabelle del database. Un articolo [@Guide_to_Microsoft_SQL_Server_Memory] suggerisce che per migliorare le prestazioni, SQL Server memorizza i dati nella cache. SQL Server sa che una volta che legge i dati dalle unità di disco, i dati non cambiano a meno che SQL Server stesso non li aggiorni. I dati possono essere letti in memoria una volta e conservati in sicurezza finché l'istanza non viene riavviata o SQL Server non decide di liberali. Se si dispone di un server con memoria sufficiente per memorizzare nella cache l'intero database, SQL Server farà proprio questo, carica tutto il database nella RAM. Allora perché SQL Server non rilascia la memoria nelle ore di picco ma lo fa soltanto quando il livello di carica del sistema diminuisce. Ci sono molte cause, ma questo articolo web [@Guide_to_Microsoft_SQL_Server_Memory] propone una guida per risolvere problemi dell'utilizzo della memoria RAM da parte di SQL server. I principali problemi sono:

- Archiviazione lenta ed economica (come dischi rigidi SATA e iSCSI da 1 Gb)
- Programmi che recuperano inutilmente troppi dati
- Database che non ha buoni indici
- CPU che non possono creare piani di query abbastanza velocemente

<!-- Tra le 4 problematiche  -->
Scartiamo subito il problema dell'archiviazione e la CPU lenta perché l'istanza di SQL Server è installata su un server performante. Rimangono due opzioni da approfondire: il middleware che recupera inutilmente troppi dati e la struttura del database che non è correttamente indicizzata. Questo documento ricopre l'approfondimento del recupero inutile di dati tenendo conto delle soluzioni applicate per migliorare l'indici dell’intero database e i piani di esecuzioni delle query. Sono stati creati nuovi indici, altri aggiornati o rimossi. Gli amministratori del database hanno creato job pianificati con lo scopo di tenere aggiornati gli indici e i piani di esecuzioni delle query. Tutto questo ha portato ad un miglioramento del sistema e ad una diminuzione della finestra del tempo di blocco del TLA, ma non ha risolto il problema. Quindi, gli amministratori di sistema hanno monitorato il database per un periodo di tempo e dai log prodotti hanno fornito informazioni sul numero totale di query richieste ed eseguite al secondo, media in secondi della esecuzione delle query, il numero di lock e la durata in secondi dei lock acquisiti e non rilasciati. Il dato più preoccupante è la quantità di lock sulla tabella 'lista_di_lavoro' che corrisponde all'entità logica liste di lavoro che è stata introdotta nella sezione @fase_pre_analitica. Di default, SQL Server utilizza il lock a livello di riga per acquisire i lock sulle righe di dati, riducendo al minimo i possibili problemi di concorrenza nel sistema. È necessario ricordare, tuttavia, che l'unica garanzia fornita da SQL Server è che applica l'isolamento e la coerenza dei dati in base ai livelli di isolamento delle transazioni. Il comportamento di blocco non è documentato e in alcuni casi SQL Server può scegliere di eseguire il lock a livello di pagina o di tabella piuttosto che a livello di riga [@Expert+SQL+Server+Transactions+and+Locking]. Il codice \ref{lst:ddl_lista_di_lavoro} rappresenta il DDL[^DDL] della tabella su quale sono stati condotti alcuni esperimenti per comprendere come SQL Server applica i lock. Questa tabella ha una chiave primaria in clustered nella colonna `IDALISTADILAVORO` con un indice tipo cluster definito. Il \ref{lst:sql_1} viene utilizzata per inserire dati fittizi nella tabella. Alla riga 1-6 vengono dichiarate le variabili che stabiliscono quante chiavi primarie e chiavi esterne creare. Alla riga 8-13 viene generata una tabella `IDs` che contiene 65536 righe con numeri progressivi interi. Alla riga 15-25 vengono create 65536 righe nella tabella temporanea `LL` a partire dalla tabella `IDs` che contengono ulteriori dati finti per essere inseriti nella tabella `LRALISTEDILAVORO` alla riga 27-35.

\begin{lstlisting}[language=SQL, caption=DDL per la creazione della tabella LRALISTEDILAVORO, label={lst:ddl_lista_di_lavoro}]
create schema LAB;

create table LAB.LRALISTEDILAVORO
(
    IDALISTADILAVORO   int      not null identity (1,1),
    ARICHIESTAID       int      not null,
    ACONTENITOREID     int      not null,
    AANALISIID         int      not null,
    DDEVICEID          int      not null,
    DESCRIZIONE        varchar(64)  null,
    STATO              smallint not null,
    DATAORAINSERIMENTO datetime not null
        constraint DEF_LRALISTEDILAVORO_DATAORAINSERIMENTO
            default getDate(),
    constraint PK_LRALISTEDILAVORO primary key clustered (IDALISTADILAVORO)
)
\end{lstlisting}

\begin{lstlisting}[language=SQL, caption=Sql utilizzato per inserire dati fittizi nella tabella LRALISTEDILAVORO, label={lst:sql_1}]
declare
 -- Qaunte PK creare
 @MaxIDALISTADILAVORO int = 65536, 
 -- Quante FK (LRARICHIESTA) creare
 @MaxARICHIESTAID int = 1000,       
 -- Quante FK (LRACONTENITORE) creare
 @MaxACONTENITOREID int = 1500,     
 -- Quante FK (LRAANALISI) creare
 @MaxAANALISIID int = 65536,       
 -- Quante FK (LRDDEVICE) creare
 @MaxDeviceID int = 100;            

-- 2 righe
with N1(C) as (select 0 union all select 0),   
  -- 4 righe           (2 x 2)             
  N2(C) as (select 0 from N1 as T1 cross join N1 as T2),  
  -- 16 righe          (4 x 4)
  N3(C) as (select 0 from N2 as T1 cross join N2 as T2),
  -- 256 righe        (16 x 16)  
  N4(C) as (select 0 from N3 as T1 cross join N3 as T2),  
  -- 65536 righe     (256 x 256) 
  N5(C) as (select 0 from N4 as T1 cross join N4 as T2),  
  IDs(ID) as (select row_number() over (order by (select null)) from N5),

  LL(IDALISTADILAVORO, ARICHIESTAID, ACONTENITOREID, AANALISIID, DDEVICEID, STATO, DATAORAINSERIMENTO)
    as
    (select ID,
            ID % @MaxARICHIESTAID + 1,
            ID % @MaxACONTENITOREID + 1,
            ID % @MaxAANALISIID + 1,
            ID % @MaxDeviceID + 1,
            ID % 5,
            dateadd(minute, -ID % (365 * 24 * 60), getdate())
    from IDs
    where ID <= @MaxIDALISTADILAVORO)

insert
into LAB.LRALISTEDILAVORO(ARICHIESTAID, ACONTENITOREID, AANALISIID, DDEVICEID, STATO, DATAORAINSERIMENTO)
select o.ARICHIESTAID,
       o.ACONTENITOREID,
       o.AANALISIID,
       o.DDEVICEID,
       o.STATO,
       o.DATAORAINSERIMENTO
from LL o;
\end{lstlisting}

Il codice \ref{lst:sql_2} utilizza la vista `sys.dm_tran_locks`, che restituisce informazioni sulle richieste di acquisizione di lock correnti nel sistema. Nelle sue query, il middleware utilizza spesso il livello di isolamento `READ UNCOMMITTED`, quindi viene impostato lo stesso livello nel pezzo di codice. Eseguiamo il codice e controlliamo quali lock vengono mantenuti dopo l'aggiornamento di una riga nella tabella. 

\begin{lstlisting}[language=SQL, caption=Aggiornamento di una riga e controllo dei lock acquisiti, label={lst:sql_2}]
set transaction isolation level read uncommitted
begin tran
update LAB.LRALISTEDILAVORO
set DESCRIZIONE = 'New Reference'
where IDALISTADILAVORO = 100;
select l.resource_type
    , case
          when l.resource_type = 'OBJECT'
              then
              object_name
                  (
                  l.resource_associated_entity_id
                  , l.resource_database_id
                  )
          else ''
  end as [table]
    , l.resource_description
    , l.request_type
    , l.request_mode
    , l.request_status
from sys.dm_tran_locks l
where l.request_session_id = @@spid;
commit
\end{lstlisting}

La figura @fig:sql_result_1 mostra l'output dell'istruzione SELECT. SQL Server ha mantenuto un lock esclusivo (X) sulla riga della chiave primaria e lock `intent exclusive` (IX) sia sulla pagina che sull'oggetto (tabella). I lock `intent exclusive` (IX) indicano l'esistenza del `lock exclusive` mantenuto a livello di riga. La colonna `resource_description` indica le risorse su cui i lock sono stati acquisiti. Per la pagina, indica la sua posizione fisica (file 1, pagina 1962 nel database), e per la riga (KEY) indica il valore hash della chiave dell’indice. I `lock exclusive` vengono acquisiti dalle istruzioni - `INSERT`, `UPDATE`, `DELETE`, istruzioni che modificano i dati. Queste query acquisiscono il lock sulle righe interessate e lo mantengono fino alla fine della transazione. Un solo `lock exclusive` può essere acquisito alla volta e più sessioni non possono modificare gli stessi dati contemporaneamente. I livelli d'isolamento delle transazioni non influiscono sul comportamento del `lock exclusive`. Questo vale anche in modalità dell’isolamento `READ UNCOMMITTED` [@Expert+SQL+Server+Transactions+and+Locking]. Più lunghe sono le transazioni eseguite da un'applicazione e più lunghi sarebbero i `lock exclusive` mantenuti, il che aumenta la possibilità che si verifichi un blocco. 

![Lock mantenuti dopo l'istruzione `UPDATE`.](source/figures/sql_result_1.png ){#fig:sql_result_1}

Eseguendo la query \ref{lst:sql_2} e modificando l'istruzione `UPDATE` con il frammento di codice \ref{lst:sql_senza_indice} è possibile osservare (figura @fig:sql_result_3) un altro tipo di lock che SQL Server utilizza, `intent update` (IU) e `update` (U), che lo acquisisce durante la ricerca delle righe da aggiornare. Dopo che il lock `update` (U) viene acquisito, SQL Server legge la riga e valuta se la riga deve essere aggiornata controllando i dati della riga rispetto ai predicati della query. Se questo è il caso, SQL Server esegue la conversione del lock `update` a `lock exclusive` ed esegue la modifica dei dati. In caso contrario, il lock `update` viene rilasciato. Il numero di lock dipende molto anche dai piani di esecuzione delle query. Sulla colonna `DDEVICEID` non c'è nessun indice configurato e SQL Server deve eseguire una scansione `clustered index scan`, acquisendo un numero di lock del tipo `update` e `intent update` maggiore di quello realmente necessario [@7554868]. Tale comportamento ci porta a ipotizzare uno dei tipici scenari di blocco. Consideriamo una situazione dove una delle sessioni ha acquisito un `lock exclusive` su una singola riga. Se una seconda sessione dovesse aggiornare una riga diversa eseguendo un'istruzione `UPDATE` non ottimizzata, SQL Server acquisirebbe un `lock update` su ogni riga che ritiene necessario e la sessione verrebbe bloccata tentando di leggere la riga con `lock exclusive` mantenuto su di essa. Anche se la seconda sessione non ha bisogno di aggiornare quella riga, SQL Server deve ancora acquisire lock del tipo `update` per valutare se quella riga deve essere aggiornata. Dobbiamo precisare che i lock `intent update` diventano del tipo `lock update` e i lock `intent exclusive` diventano `lock exclusive`.

\begin{lstlisting}[language=SQL, caption=Query per aggiornare la colonna `DESCRIZIONE` utilizzando come predicato una colonna senza indice., label={lst:sql_senza_indice}]
update LAB.LRALISTEDILAVORO
set DESCRIZIONE = 'New device changed description'
where DDEVICEID in (1);
\end{lstlisting}

![Sezione dei lock mantenuti dopo l'istruzione `UPDATE`.](source/figures/2022-11-27-11-53-55.png){#fig:sql_result_3}

Il risultato della figura @fig:sql_result_select mostra i lock `share` e i lock `intent share` acquisiti dalle sessioni `SPID=51` e `SPID=52` eseguendo le query \ref{lst:sql_select}. Come si può notare, entrambe le sessioni hanno acquisito i lock sulle stesse pagine e righe della tabella `LRALISTEDILAVORO` senza bloccare l'altra. L'isolation level è impostato a `repeatable read` che garantisce che le altre sessioni non possono modificare il dato dopo che è stato letto. Impostando isolation level a `READ UNCOMMITTED`, il lock `share` non viene acquisito e può generare i `dirty read` perché il dato letto in precedenza da una sessione potrebbe essere modificato da una seconda sessione. Nel caso del lock `exclusive`, l'acquisizione non dipende mai dal livello di transazione.

\begin{lstlisting}[language=SQL, caption=Query per la selezione delle righe utilizzando come predicato una colonna senza indice., label={lst:sql_select}]
-- Start session (SPID=51)
set transaction isolation level repeatable read
begin tran
select *
from LAB.LRALISTEDILAVORO
where LRALISTEDILAVORO.DDEVICEID = 1;
--  

-- Start session (SPID=52)
set transaction isolation level repeatable read
begin tran
select *
from LAB.LRALISTEDILAVORO
where LRALISTEDILAVORO.DDEVICEID = 1;
select l.resource_type
     , l.resource_description
     , l.request_type
     , l.request_mode
     , l.request_status
from sys.dm_tran_locks l
where l.request_session_id = @@spid;

-- End session (SPID=52)
commit;

-- End session (SPID=51) 
commit;
\end{lstlisting}

![Lock mantenuti dopo l'istruzione `SELECT` delle sessioni SPID=51 e SPID=52.](source/figures/2022-11-27-14-28-55.png){#fig:sql_result_select}

L'analisi è stata fatta perché il middleware nelle transazioni utilizza diversi tipi di isolation level. L'analisi è stata utile per trovare una soluzione migliore e un utilizzo chiaro delle transazioni che sarà approfondita nella \+@sec:modifiche_transazioni

## Impatto del flusso di Messaggi HL7 e ASTM su\break SQL Server

Andiamo ora ad analizzare come il flusso di messaggi che scorrono da e verso il middleware, impatta in modo negativo il SQL Server e il sistema in generale. La figura @fig:architettura_middleware mostra l'architettura a microservizi del middleware e i tipi di messaggi scambiati tra le varie entità. Il servizio Front End gestisce tutte le interazioni degli analisti di laboratorio, il LIS Back End gestisce tutti i flussi da LIS mentre Device Back End gestisce tutti i flussi dagli strumenti del laboratorio. Questi servizi sono collegati al Sql server e implementano `optimistic concurrency` per gestire la concorrenza. I servizi `Driver` sono moduli specializzati ad eseguire compiti molto specifici. Ci sono i driver che comunicano solo con il LIS, altri con l'automazione o con diversi strumenti di analisi in vitro prodotti da aziende diverse. I servizi possono essere avviati su macchine diverse e la comunicazione viene fatta tramite il sistema architetturale REST.

![Architettura a microservizi del Middleware.](source/figures/architettura_middleware.png){#fig:architettura_middleware}

Nel laboratorio di riferimento Labo L, il numero di campioni processati al giorno si aggira intorno ai 2000, mentre nel Labo Centrale i campioni processati sono in media 10000. Il volume di lavoro del Labo Centrale è 5 volte più grande che quello del Labo L. Analizzando le figure @fig:message_type_burst e @fig:message_type_burst_tot_percent, possiamo osservare un incremento del flusso di messaggi inviati al middleware, dalle ore 10 fino alle ore 15, che è il 60% del totale numero di messaggi inviati in un arco di 24 ore. Inoltre, quando il sistema del Labo Centrale rallenta, è stata osservata una moltiplicazione di messaggi a causa del timeout delle richieste. I dispositivi dell'automazione sono configurati in modo tale da rinviare i messaggi non riscontrati.

![Numero medio di messaggi inviati al middleware.](source/figures/message_type_burst.png){#fig:message_type_burst width=80%}

![Frequenza dei messaggi inviati al middleware.](source/figures/message_type_burst_tot_percent.png){#fig:message_type_burst_tot_percent width=80%}

<!-- 
Parlare anche di pessimistic e optimistic concurrency e come è stata applicato.

You can resolve this problem by optimizing the query and adding the index on the 
DDEVICEID column, which will replace the Clustered Index Scan with the Nonclustered 
Index Seek operator in the execution plan. This will significantly reduce the number of 
locks the statement acquires and eliminate lock collision and blocking as long as the 
queries do not delete and select the same rows.
--> 

<!-- 
presentare la struttura del database. ok
presentare i componenti software, driver printer labreboot.
presentare il numero di messaggi e il tipo di messaggi che middleware gestisce
presentre cosa potrebbe leggere troppo dal database
presentare quello che potrebbe scrivere troppo
arivare ai lock sulle tabelle, sulla lista di lavoro. 
presentare 

Iniziare con l'analisi di messaggi scritti sul database.
--> 

<!-- 
Nella tabella @tbl:2 vengono presentiti i numeri di messaggi giornalieri 

-------------------------------------------------------------
Origine o tipologia          N° messaggi    Media di esecuzione 
                                            di un messaggio
---------------------------- -------------- -----------------
Anagrafica (LIS)             70000          500 ms

Accettazioni (LIS)           10000          2000 ms

Aggiornamenti (LIS)          1000           n/a

Dispositivi di pre-analitica 10000          100  ms    

PowerExpress                 400000         100  ms

Procedure analitiche         40000          2000 ms 

Totale                       500000          n/a
-------------------------------------------------------------

: Numeri di messaggi inviati al middleware e media di esecuzione di ogni messaggio. {#tbl:2}

-------------------------------------------------------------
Operazione o funzionalità                   Media di esecuzione 
------------------------------------------ ------------------
Parser                                      500 ms

Lexer                                       2000 ms

Operazioni di controllo sui dati            1000

Inoltro del messaggio a IntraLab
-------------------------------------------------------------

: Misurazioni del percorso di un messaggio e quanto tempo impiega per arrivare sul db. {#tbl:3}

--> 

<!-- ## Refactoring dell'architettura del middleware -->

<!--## Pre refactoring--> 

<!-- 
Iniziare a spiegare il basso livello TCP/IP 
vari livelli di comunicazione. livelli con errori e senza errori
--> 

<!-- 

Guarda 3.2. Architecture
https://www.sciencedirect.com/science/article/pii/S0169260717311707#bib0015

--> 
<!--## Ricerca e analisi documentale--> 
<!-- 
Il middleware era deploy come due istanze. La prima istanza serve le operazioni per le analisti di laboratorio. Gli operatori eseguono operazioni di consultazione, creazione di report e validazione delle analisi eseguiti. Anche le regole di validazione vengono eseguite su questa istanza. La seconda istanza serve ai collegamenti con i driver. L'automazione e collegata al middleware tramite un apposito driver. Esistono driver specifici per ogni singolo device nel laboratorio che deve essere pilotato dal middleware. Per esempio cela frigo è collegato al driver che è in grado di inviare e ricevere messaggi per lo stoccaggio delle provette e il loro successivo recupero nel caso in cui si deve eseguire ulteriori analisi sul campione. Descrive l'architettura

L'ipotesi principale del problema del blocco di sistema nelle ore di picco, era l' insufficienza del potere di calcolo. Come soluzione è stato deciso di aumentare l'istanze del middleware portando a 4 le istanze per il Back end e a 2 le istanze per il front end. Il problema subito emersa, era l'inconsistenza di alcuni dati nel database relazionale e documentale a causa della race condition creata tra le istanze. Per esempio i risultati di due analisi diverse (HDL e HLL TODO) dello stesso campione arrivavano da due strumenti diversi collegati a due istanze diverse.

é stato necessario l'implementazione di una soluzione di transazioni distribuite.

a) http://osl.cs.illinois.edu/media/papers/astley-2001-cacm-customizable_middleware_for_modular_distributed_software.pdf

b) algoritmo redlock


!!!Descrivi una situazione lettura scrittura con la consistenza di dati!!!
!!!Parla del algoritmo RED LOCK implementato e presenta una situazione dove questo capitava!!!
!! Presenta come una scrittura viene eseguita sul database !!

--> 


<!--## Teorie e algoritmi--> 

<!--## Prototipo dell'architettura--> 
