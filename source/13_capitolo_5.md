# Tecnologie utilizzate {#tu}

In questo capitolo verranno trattate le tecnologie utilizzate durante il tirocinio per apportare modifiche al progetto in essere e per sviluppare un nuovo modulo software che cerca di risolvere il problema dei blocchi della TLA. Per sviluppare la soluzione il tirocinante ha utilizzato i servizi web basati sui messaggi, code di messaggi e le strutture dati di Redis.

## ServiceStack

Il middleware è stato sviluppato in C#/.NET utilizzando il framework ServiceStack. Per approcciare ad un progetto cosi complesso è stato necessario uno studio approfondito della architettura del middleware e del framework utilizzato. Verano affrontati le principali peculiarità di ServiceStack, e come funziona in contrasto ad altri modelli e quali sono i principali vantaggi e svantaggi. Inoltre, verranno presentati i principi di base e le funzionalità non ancora utilizzate dal progetto in essere e che si sono rilevati vincenti nella costruzione del prototipo. 

ServiceStack è un framework utilizzato per creare servizi web basati sui messaggi (message-based). In sostanza, un servizio basato sui messaggi è un servizio che trasmette messaggi per facilitare la comunicazione. Una buona metafora per illustrare le differenze tra un metodo RPC e un'API basata sui messaggi può essere vista nel meccanismo di invio dei messaggi di Smalltalk o Objective-C [@objective-c-message-dispatch-mechanism] rispetto a una normale chiamata al metodo statico del linguaggio C. Le chiamate ai metodi sono accoppiate all'istanza su cui sono invocate. In un sistema basato su messaggi la richiesta viene catturata nel messaggio e inviata a un destinatario. Il destinatario non deve gestire il messaggio in quanto può facoltativamente delegare la richiesta a un destinatario alternativo per l'elaborazione [@interview-servicestack-2]. 

Vediamo un confronto tra un servizio web progettato con le tecnologie RPC[^RPC] e un servizio web basato sul DTO[^DTO], il design pattern di Martin Fowler. I servizi web che adottano l'approccio RPC sono progettati in tal modo che le richieste assomigliano a una chiamata di funzione:

\begin{lstlisting}[language = {[Sharp]C}, caption=Esempio servizio RPC]
public interface IHbService {
  string ClcHb(double input);
  string ClcHbNormalValue(double input);
}
\end{lstlisting}

Questo approccio tende a rendere il servizio web più suscettibile alle modifiche di interfaccia che possono rompere il contratto. Ad esempio, se nel frammento di codice precedente, una versione successiva del servizio web richiede altri input dal client per eseguire il metodo `ClcHb` o deve restituire un altro campo oltre al valore della stringa, è inevitabile una modifica sostanziale a tutti i client che utilizzano il servizio. Naturalmente, è sempre possibile creare una versione v2 del metodo `ClcHb` parallelo per accettare ulteriori argomenti di input, ma nel tempo questo riempirebbe l'interfaccia del servizio web e rischierebbe di confondere i consumatori. Il frammento di codice successivo mostra come trasformare il servizio web `IHbService` basato su RPC in un servizio web basato sul DTO.

\begin{lstlisting}[language = {[Sharp]C}, caption=Esempio di un servizio basato sul DTO]
public class ClcHbValueRequest {
  public double Input { get; set; }
}

public class ClcHbNormalValueRequest {
  public double Input { get; set; }
  public int Normalizer { get; set; }
}

public class HbResponse {
  public string Result { get; set; }
}

public interface IHbService {
  HbResponse Get(ClcHbValueRequest request);
  HbResponse Get(ClcHbNormalValueRequest request);
}
\end{lstlisting}

Ogni metodo del servizio web riceve nel input la richiesta DTO con un nome indistinguibile e restituisce una singola risposta DTO. Se si aggiungono nuovi campi al DTO di richiesta o di risposta, questo non dovrebbe rompere il contratto. ServiceStack applica gli standard basati sulla convenzione DTO e best practice per i servizi web remoti per la sua interfaccia dei servizi web. In questo modo, ServiceStack, può garantire per ogni servizio web il supporto alle code di messaggi. Torneremo a pararle dei servizi web e code di messaggi in questo capitolo. Utilizzare il DTO per la progettazione dei servizi web incoraggia l'uso del principio Low Coupling e High Coesion [@servicestackdoc]. Spesso si vuole progettare componenti che siano autonomi: indipendenti e con un unico scopo ben definito [@hunt2000pragmatic]. In ServiceStack tutti i DTO definiti dal utente devono stare in librerie o assembly che non contengono alcun tipo di implementazione. Queste librerie sono facilmente condivisibili con altri clienti .NET che non devono installare altre dipendenze. Inoltre ServiceStack può generare i tipi per altri linguaggi come `Typescript`, `JavaScript`, `Java`, `Swift`, `Dart`, `python` a partire da una libreria `.NET` che contiene i DTO. In questo modo, i client progettati in linguaggi diversi possono avere accesso ai contratti e alla documentazione dei servizi web di un'applicazione basata sul framework ServiceStack. 

Rivediamo l'esempio del servizio `IHbService` in un'implementazione reale con ServiceStack. Alle classi del tipo `Request` viene aggiunto l'attributo `Route` che il framework, utilizzando la reflection, mappa in una tabella il path del servizio web, il nome del DTO e l'istanza del servizio che deve gestire la richiesta specifica. 

\begin{lstlisting}[language = {[Sharp]C}, caption=Configurazione di esempio del DTO con ServiceStack]
public interface IHasInput {
  double Input { get; set; }
}

public interface IHasInputNormalizer : IHasInput {
  int Normalizer { get; set; }
}

[Route("api/calculate/hemoglobin-value")]
public class ClcHbValueRequest : IHasInput {
  public double Input { get; set; }
}

[Route("api/calculate/hemoglobin-normal-value")]
public class ClcHbNormalValueRequest : 
  ClcHbValueRequest, 
  IHasInputNormalizer {
  public int Normalizer { get; set; }
}
\end{lstlisting}

Il servizio `HbService` estende la classe di base `Service` di ServiceStack e implementa l'interfaccia `IHbService`. Un'istanza dell'implementazione di `IHbCalculator` viene iniettata tramite la funzionalità di dependency injection (DI) del framework. La dependency injection è una forma specializzata del design pattern Inversion of Control (IoC) di Martin Fowler [@ioc_mf]. ServiceStack implementa una sua versione del IoC che è rappresentato da un grafo di dipendenze che descrive in che modo i componenti devono essere inizializzati e in che ordine.

\begin{lstlisting}[language = {[Sharp]C}, caption=Servizio web basato sui messaggi con ServiceStack]
public class HbService : Service, IHbService {
  private IHbCalculator _hbCalculator;
  public HbService(IHbCalculator hbCalculator) {
    _hbCalculator = hbCalculator;
  }
  HbResponse Get(ClcHbValueRequest request) 
    => new HbResponse{ Result = _hbCalculator.Value(request) };
  HbResponse Get(ClcHbNormalValueRequest request) 
    => new HbResponse{ Result = _hbCalculator.Value(request) };
}
public interface IHbCalculator {
  string Value(IHasInput request);
  string Value(IHasInputNormalizer request);
}
\end{lstlisting}

\noindent 
Un client può invocare i metodi di questo servizio web in due modi:

```http
GET: 
https://labware.com?Action=ClcHbNormalValueRequest
&Input=98.99
&Normalizer=5
```

\noindent 
oppure in modo classico
```http
https://labware.com/api/calculate/hemoglobin-normal-value
&Input=98.99
&Normalizer=5
```

`Action` è un metodo di ServiceStack che accetta un verbo HTTP: `GET, PUT, POST, DELETE` e un qualsiasi DTO rappresentato dal suo nome e i suoi campi se necessario. Se guardiamo [l'esempio delle API del servizio web EC2 di Amazon](https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_AttachVolume.html)[@api_amazon_sample], osserviamo una forte somiglianza con l'approccio di ServiceStack in cui viene invocato un messaggio di richiesta e restituito un messaggio di risposta. Anche Google utilizza un meccanismo simile per la comunicazione dei loro microservizi ad uso interno [@protocol_buffers]. Utilizzando questo approccio possiamo notare la facilità con cui è possibile implementare una soluzione Low Coupling e High Coesion così come viene mostrato nella figura @fig:dtos-role. Questo modello offre la possibilità di aggiungere con facilità microservizi e sistemi distribuiti che sono facilmente implementabili e testabili. Lo sviluppatore si deve concentrare di trovare una soluzione al problema e non all'architettura da implementare. 

![Rollo del DTO nel collegamento tra un client e il service in un'architettura basata sui messaggi](source/figures/dtos-role.png){#fig:dtos-role}

## Servizi web asincroni basati sui messaggi {#swabsm}

Sappiamo che il modello client-server basato sul protocollo HTTP è un modello sincrono e anche i servizi web basati sui messaggi lo sono. Però, ServiceStack fornisce un'API di code di messaggi di alto livello che espone una serie di funzionalità essenziali per pubblicare e consumare messaggi, dando la possibilità di trasformare un semplice servizio web sincrono in uno asincrono. Questo tipo di funzionalità non è stata presa in considerazione nel progetto del LabWare e mancava tutta la configurazione per abilitarla. Grazie al design pattern DTO, il framework ServiceStack ha incoraggiato la separazione dei concetti che ha reso possibile la trasformazione dei servizi web esistenti del middleware riutilizzabile in un contesto come servizi MQ[^mq]. 

In breve, la richiesta DTO di un servizio web viene gestita da un produttore di messaggi che incapsula il DTO nel corpo di un oggetto che implementa l'interfaccia `IMessage`. Il messaggio viene inserito in una coda di messaggi gestita da un broker MQ, così come nella figura @fig:servicestack-mqclients. Il messaggio successivamente viene consumato da un consumatore che è rappresentato dall'implementazione del servizio web che prende in input la richiesta DTO, la processa e produce una risposta DTO che a sua volta viene gestita da un produttore e un broker MQ. Il tirocinante ha sfruttato la funzionalità dei servizi MQ per implementare una soluzione personalizzata per il middleware LabWare che verrà presentata nel prossimo capitolo.

![Principali componenti dell'architettura dei servizi MQ e servizi web asincroni basati sui messaggi](source/figures/servicestack-mqclients.png){#fig:servicestack-mqclients}

<!-- 
https://docs.servicestack.net/why-remote-services-use-dtos#the-service-layer-is-your-most-important-contract
https://docs.servicestack.net/service-complexity-and-dto-roles

PUNTI NEGATIVI dto con nome tyroppo lunghi

PARLA DI ALTRI BENEFICI.
https://www.infoq.com/articles/interview-servicestack/
: parala di facade e di Data Transfer Object (MSDN)
Nel nostro caso, questo approccio per lo sviluppo di un prototipo che correggere alcuni problemi di performance 

Parla come questo meccanismo di code non è stato sfruttato al meglio: 
La maggior parte dei servizi web sviluppati nel middleware sono servizi web basati sui messaggi. Questi servizi sono stati aggiunti come tali perché il ServiceStack ha imposto uno standard della scrittura del codice per i servizi. Non necessariamente vuol dire che il middleware ha sfruttato tutte le potenzialità del meccanismo offerto dal framework per i message-based web services. (Continua)
-->

## Redis

Redis è diventato il database chiave-valore più conosciuto e utilizzato per le operazioni di caching e non solo[@redis_rank]. Creato da Salvatore Sanfilippo e sponsorizzato da Redis, nei benchmark è classificato come il database in-memory[^in-memory] più veloce [@redis_loved]. LabWare utilizza Redis per fare il caching di alcuni risultati delle query eseguite nel database SQL Server. Tramite le strutture dati, le funzioni e il time to live (TTL) che Redis offre, è possibile creare caching molto complessi e auto gestiti. ServiceStack ha implementato un client in .NET che si interfaccia con Redis [@redis_client]. La libreria mette a disposizione un'astrazione delle strutture dati e l'accesso a tutti i comandi di Redis. Redis offre anche un meccanismo di publisher/subscriber @redis_pub_sub che con l'utilizzo della struttura dati `List` è possibile creare un broker MQ.

### Insiemi ordinati in Redis

La struttura dati `ZSET` può essere paragonata a un dizionario ordinato in base ai valori delle chiavi (vedi \+@fig:zset). Abbinando la struttura dati `LIST` con `ZSET` è possibile creare una coda con priorità.

![Un esempio di ZSET con quattro membri/punteggi](source/figures/zset.png){#fig:zset width=65%}

\noindent 
Alcuni comandi di base di una struttura dati `ZSET`:

+ `ZADD` aggiunge un nuovo membro alla collezione mantenendo l'univocità e l'ordine.
+ `ZRANGE` restituisce i membri di un insieme ordinato, ordinati all'interno di un dato intervallo.
+ `ZREMRANGEBYSCORE` rimuove da un insieme ordinato l'elemento con il peso specificato.

La maggior parte dei comandi della struttura dati `ZSET` hanno una complessità temporale $O(log(n))$, dove $n$ è il numero degli elementi della collezione. Questa struttura dati potrebbe risolvere alcuni problemi di performance andando a sostituire le query che il middleware esegue per aggiornare il panello di monitoraggio del andamento del lavoro nel laboratorio. 

### Transazioni in Redis

Un'altra funzionalità di Redis che è stata utilizzata dal tirocinante sono le transazioni. Una transazione Redis permette di eseguire un gruppo di comandi in un singolo passo. Tutti i comandi in una transazione vengono serializzati ed eseguiti in sequenza e una richiesta di un altra sessione non potrà interporsi in mezzo al'esecuzione di questa serie di comandi. Questo è diverso da una transazione di database relazionale, che può essere eseguita parzialmente e quindi sottoposta a rollback o commit. Redis non interromperà l'elaborazione dei comandi in una transazione quando un comando fallisce [@redis_transaction]. L'implementazione di un algoritmo di lock distribuito che utilizza le transazioni di Redis verrà mostrato nella \+@sec:redlock.

## Docker

Docker è una piattaforma che ti consente di "creare, spedire ed eseguire qualsiasi applicazione, ovunque" [@docker_overview]. Prima dell'arrivo di Docker, la pipeline di sviluppo in genere prevedeva la combinazione di varie tecnologie per la gestione del movimento di software, come ad esempio macchine virtuali, strumenti di gestione della configurazione, sistemi di gestione dei pacchetti, e complesse reti di dipendenze di librerie. Tutti questi strumenti dovevano essere gestiti e mantenuti da figure specializzati, e la maggior parte aveva i propri modi unici di creare la configurazione. In questo tirocinio è stato utilizzato Docker per tenere separate le configurazioni necessarie per utilizzare Redis e SQL Server in locale senza la necessita di accedere a istanze esistenti nella rete aziendale. A differenza delle macchine virtuali, Docker necessita meno memoria RAM, meno spazio sul hard disk e meno configurazioni. 

### Immagini e Contenitori

I concetti fondamentali di Docker sono le immagini e i contenitori. Se si ha familiarità con i principi della programmazione orientata agli oggetti, un altro modo di guardare le immagini e i contenitori è di visualizzare le immagini come classi e i contenitori come oggetti. Nello stesso modo che gli oggetti sono istanze concrete delle classi, i contenitori sono istanze delle immagini. Si possono creare più contenitori da una singola immagine e sono tutti isolati da uno un altro nello stesso modo in cui sono gli oggetti. Qualunque cosa tu cambi nell'oggetto, non influirà la definizione di classe. 

### Configurazione utilizzata

Per iniziare subito con la prototipazione di una soluzione è stato creato un docker file che descrive tutte le librerie necessarie per l'avvio dei microservizi essenziali del middleware. Dal hub di docker [@microsoft-dotnet-sdk] è possibile scaricare le immagini con un ambiente preconfigurato dove è possibile sviluppare, buildare e testare applicazioni in dotnet. In seguito un esempio del docker file:


``` dockerfile
# immagine contenente l'ambiente di sviluppo di dotnet v6.0
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env

# Compila e avvia WireLab (Middleware)
RUN cd /var/WireLab
RUN dotnet build WireLab.sln
RUN dotnet output/WireLab.dll

# Compila e avvia WireDriver per Sangue
RUN cd /var/WireDriver
RUN dotnet build WireDriver.sln --configuration Sangue -o DriverSangue
RUN dotnet DriverSangue/WireDriver.dll

# Compila e avvia WireDriver per Urine
RUN cd /var/WireDriver
RUN dotnet build WireDriver.sln --configuration Urine -o DriverUrine
RUN dotnet DriverUrine/WireDriver.dll
```

Utilizzando il commando docker-compose è possibile avviare piu contenitori utilizzando la configurazione descritta in un file `docker-compose.yml`. In questo file il tirocinante ha definito in che modo devono essere avviati i contenitori e la rete utilizzata per la comunicazione. 