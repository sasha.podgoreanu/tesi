create schema LAB;

create table LAB.LRALISTEDILAVORO
(
    IDALISTADILAVORO   int      not null identity (1,1),
    ARICHIESTAID       int      not null,
    ACONTENITOREID     int      not null,
    AANALISIID         int      not null,
    DDEVICEID          int      not null,
    DESCRIZIONE        varchar(64)  null,
    STATO              smallint not null,
    DATAORAINSERIMENTO datetime not null
        constraint DEF_LRALISTEDILAVORO_DATAORAINSERIMENTO
            default getDate(),
    constraint PK_LRALISTEDILAVORO primary key clustered (IDALISTADILAVORO)
)

declare
 @MaxIDALISTADILAVORO int = 65536,
 @MaxARICHIESTAID int = 1000,
 @MaxACONTENITOREID int = 1500,
 @MaxAANALISIID int = 65536,
 @MaxDeviceID int = 100;

with N1(C) as (select 0 union all select 0),
    N2(C) as (select 0 from N1 as T1 cross join N1 as T2),
    N3(C) as (select 0 from N2 as T1 cross join N2 as T2),
    N4(C) as (select 0 from N3 as T1 cross join N3 as T2),
    N5(C) as (select 0 from N4 as T1 cross join N4 as T2),
    IDs(ID) as (select row_number() over (order by (select null)) from N5),

    LL(IDALISTADILAVORO, ARICHIESTAID, ACONTENITOREID, AANALISIID, DDEVICEID, STATO, DATAORAINSERIMENTO)
         as
         (select ID,
                 ID % @MaxARICHIESTAID + 1,
                 ID % @MaxACONTENITOREID + 1,
                 ID % @MaxAANALISIID + 1,
                 ID % @MaxDeviceID + 1,
                 ID % 5,
                 dateadd(minute, -ID % (365 * 24 * 60), getdate())
          from IDs
          where ID <= @MaxIDALISTADILAVORO)

insert
into LAB.LRALISTEDILAVORO(ARICHIESTAID, ACONTENITOREID, AANALISIID, DDEVICEID, STATO,
                          DATAORAINSERIMENTO)
select o.ARICHIESTAID,
       o.ACONTENITOREID,
       o.AANALISIID,
       o.DDEVICEID,
       o.STATO,
       o.DATAORAINSERIMENTO
from LL o;

set transaction isolation level read uncommitted
begin tran
update LAB.LRALISTEDILAVORO
set DESCRIZIONE = 'New Reference'
where IDALISTADILAVORO = 100;
select l.resource_type
     , case
           when l.resource_type = 'OBJECT'
               then
               object_name
                   (
                    l.resource_associated_entity_id
                   , l.resource_database_id
                   )
           else ''
    end as [table]
     , l.resource_description
     , l.request_type
     , l.request_mode
     , l.request_status
from sys.dm_tran_locks l
where l.request_session_id = @@spid;
commit

-- Find objects in a particular database that have the most
-- lock acquired. This sample uses AdventureWorksDW2012.
-- Create the session and add an event and target.
IF EXISTS(SELECT * FROM sys.server_event_sessions WHERE name='LockCounts')
    DROP EVENT session LockCounts ON SERVER;
GO
DECLARE @dbid int;

SELECT @dbid = db_id();

DECLARE @sql nvarchar(1024);
SET @sql = '
    CREATE event session LockCounts ON SERVER
        ADD EVENT sqlserver.lock_acquired (WHERE database_id ='
            + CAST(@dbid AS nvarchar) +')
        ADD TARGET package0.histogram(
            SET filtering_event_name=''sqlserver.lock_acquired'',
                source_type=0, source=''resource_0'')';

EXEC (@sql);
GO
ALTER EVENT session LockCounts ON SERVER
    STATE=start;
GO
-- Create a simple workload that takes locks.

USE master;
GO
SELECT TOP 1 * FROM LAB.LRALISTEDILAVORO;
GO
-- The histogram target output is available from the
-- sys.dm_xe_session_targets dynamic management view in
-- XML format.
-- The following query joins the bucketizing target output with
-- sys.objects to obtain the object names.

SELECT name, object_id, lock_count
    FROM
    (
    SELECT objstats.value('.','bigint') AS lobject_id,
        objstats.value('@count', 'bigint') AS lock_count
        FROM (
            SELECT CAST(xest.target_data AS XML)
                LockData
            FROM     sys.dm_xe_session_targets xest
                JOIN sys.dm_xe_sessions        xes  ON xes.address = xest.event_session_address
                JOIN sys.server_event_sessions ses  ON xes.name    = ses.name
            WHERE xest.target_name = 'histogram' AND xes.name = 'LockCounts'
             ) Locks
        CROSS APPLY LockData.nodes('//HistogramTarget/Slot') AS T(objstats)
    ) LockedObjects
    INNER JOIN sys.objects o  ON LockedObjects.lobject_id = o.object_id
    WHERE o.type != 'S' AND o.type = 'U'
    ORDER BY lock_count desc;
GO

-- Stop the event session.

ALTER EVENT SESSION LockCounts ON SERVER
    state=stop;
GO
