# Il metodo sviluppato {#metodo_sviluppato}

Il tirocinio è stato svolto in diverse fasi nei quali sono stati raggiunti diversi obiettivi. Nella fase iniziale, al tirocinante sono stati presentati gli strumenti e l'ambiente di sviluppo, il team che si occupa dell'analisi di nuove funzionalità, il team di sviluppo, il team di test e il team di deployment del middleware. Sono stati introdotti i concetti fondamentali del dominio del laboratorio di analisi in vitro e del middleware. Nella seconda fase, per eseguire l'analisi del blocco di TLA, il tirocinante è stato appoggiato e seguito dal team di analisi e dell'infrastruttura. Completata l'analisi del problema, segue la terza fase dove il tirocinante ha fatto degli analisi tecniche delle possibile soluzioni. Nella quarta fase vengono apportate modifiche al software su un branch di git separato. In questa fase, il team di sviluppo ha revisionato il codice e ha indirizzato il tirocinante a rispettare delle linee guida adottate nel progetto. Nel'ultima fase, della sperimentazione, il team di test ha seguito il tirocinante nella scrittura di "unit test", "test di integrazione", "test end to end" e "stress test". Ogni volta che sono stati raggiunti gli obiettivi si è ritornato nella fase di analisi della prossima modifica, passando per le fasi di sviluppo e di test.

## Modifiche delle transizioni {#modifiche_transazioni}

Per garantire l'atomicità, la coerenza, l'isolamento e la durabilità dei dati nel database delle operazioni di lettura e scrittura sono state utilizzate le transazioni sql. Ma dall'analisi che è stata presentata nella \+@sec:sql_server_analisi ha fatto riflettere sul fatto di un sovratulizizzo delle transazioni sql nel progetto. Il modello utilizzato è la concorrenza pessimistica che funziona in base al presupposto che afferma che più sessioni possono accedere agli stessi dati e che vorrebbero eventualmente modificarli e quindi ignorare le modifiche reciproche. Il database impedisce che cio accada bloccando i dati per la durata della transazione non appena la prima sessione accede e modifica i dati. Un modello alternativo è la concorrenza ottimistica che presuppone che più sessioni possono accedere agli stessi dati, ma la possibilità della sovrascrittura dei dati è bassa. I dati non sono bloccati e quindi le eventuali scritture simultanee innescano conflitti del tipo scrittura-scrittura e il database restituisce un errore specifico a una delle due sessioni. Per abilitare il lock ottimistico in SQL Server basterebbe specificare l'isolation level delle transizioni come `READ COMMITTED` quando si fa una lettura di dati o `SNAPSHOT` quando si fa una scrittura o aggiornamento dei dati. Il middleware, per come è stato progettato, può essere utilizzato anche con il database PostgreSQL e in produzione ci sono alcuni laboratori di piccole dimensioni che utilizzano questa configurazione. Per mantenere la compatibilità si è deciso di sviluppare una soluzione software. 

### Implementazione

La modifica da fare al codice è relativamente semplice, come si può notare nell blocco di codice \ref{lst:update_c}, ma impatta su tutte le chiamate di aggiornamento di un oggetto. In breve, alla riga 2 e 3 vengono inizializzate le variabili utilizzate nella costruzione della query di aggiornamento e alla riga 5 viene invocata la chiamata di aggiornamento. Se alla riga 5 viene restituito il valore 0 vuol dire che l'aggiornamento non è stato eseguito dal database perché il valore della colonna `rowVersion` è stato cambiato da parte di una sessione concorrente che ha modificato o cancellato l'oggetto. Alla riga 10 viene lanciata un'eccezione se l'aggiornamento non è andato a buon fine. Il chiamante della funzione `updateObject`, può decidere di catturare e gestire l'eccezione ricaricando la risorsa con i campi aggiornati ed eseguire nuovamente le logiche sul dato oppure a sua volta, propagare l'errore al suo chiamante. L'eccezione può arrivare fino al servizio o l'utente che ha iniziato la chiamata. Nella prima versione l'eccezione non veniva catturata, ma dopo la sperimentazione si è deciso di gestirla il prima possibile. Questo lavoro è stato preso in carico dal team di sviluppo.

\begin{lstlisting}[language = {[Sharp]C}, caption=funzione di base per aggiornamento di un oggetto nel database, label={lst:update_c}]
int updateObject(T obj, IDbConnection dbConnection) {
  uint id = obj.id
  uint rowVersion = obj.rowVersion
  obj.rowVersion++ // Incrementa la versione del oggetto
  // Aggiorna tutti i campi dell'oggetto
  int updated = dbConnection.update(
    obj,
    row => ( row.id == id && row.rowVersion == rowVersion ))

  if (updated == 1) return 1
  else throw OutOfDateException(typeof(T), id, rowVersion)
}
\end{lstlisting}

L'applicazione è organizzata in strati ben definiti e le classi che effettuano l'accesso ai dati appartengono allo strato chiamato `Repository`. Ogni colonna del database è mappata su un'entità lato software e per ogni entità o gruppi di entità eterogenei esiste una classe `Repository` che contiene le operazioni di lettura e scrittura come ad esempio `insert(T obj)`, `update(T obj)`, `getById(uint id)` ecc. L'implementazione di una soluzione mostrata nel blocco di testo \ref{lst:update_c} è di inserirsi tra il Repository e la libreria di Servicestack utilizzata per eseguire le query. Inoltre, la soluzione permetterebbe di monitorare se la modifica è efficace e se porterebbe ad un miglioramento complessivo dell'applicazione nell'esecuzione delle query. Nella \+@fig:command_strategy viene mostrato il command e strategy pattern utilizzato per apportare le modifiche necessarie allo strato software utilizzato per modifica e accesso ai dati. 

![I pattern command e strategy utilizzati in combinazione nello strato software del repository](source/figures/command_strategy.png){#fig:command_strategy}

L'interfaccia `ISqlStatement<TResult>` è una componente del pattern strategy e rappresenta l'astrazione di un'operazione di scrittura o lettura dei dati dal database. L'interfaccia `ISqlExecute` è una componente del pattern command e rappresenta l'astrazione di un'operazione di chiamata del metodo `doInConnection` di `ISqlStatement<TResult>` e restituisce al chiamante il risultato. Nel blocco di codice \ref{lst:sql_statement_prima_dopo} viene mostrata un'implementazione e l'utilizzo dei due pattern per monitorare la durata di esecuzione delle query. Un altro problema risolto con questo pattern è di concentrare in pochi punti l'apertura e la chiusura della connessione al database. Prima della modifica venivano create e aperte un numero di connessioni pari al numero di repository. Dopo la modifica il numero delle connessioni è drasticamente sceso perché si ha solo una connessione aperta per il tempo necessario dell'esecuzione di una query. Il metodo `doInConnection` della classe `UpdateStatement` è stato implementato seguendo la metodologia mostrata nel blocco di codice \ref{lst:update_c} per gestire la concorrenza ottimistica. Con la soluzione dei due pattern è stato possibile anche un'implementazione delle transazioni basate sul isolation level per i database SQLServer e PostgreSQL delle operazioni di aggiornamento, cancellamento e inserimento di blocchi di dati.

\begin{lstlisting}[language = {[Sharp]C}, caption=Funzioni di update con e senza l'utilizzo dei pattern strategy e command, label={lst:sql_statement_prima_dopo}]
// classe base repository
class BaseRepository<TEntity> {
  IDbConnection _dbConnection // connessione con stato=Open
  ISqlExecute _sqlExecute 

  // funzione originale di update
  int update(TEntity obj, Expression<TEntity, bool> where) {
    return _dbConnection.update(obj, where)
  }

  // funzione modificata di update (strategy + command)
  int update(TEntity obj, Expression<TEntity, bool> where) {
    var statement = new UpdateStatement<int>(obj, where)
    return _sqlExecute.execute(statement);
  }
}

// classe che implementa ISqlExecute ed esegue 
// il monitoraggio dell tempo di esecuzione delle query 
class MonitorSqlExecute : ISqlExecute {
  IDbConnectionProvider _dbConnectionProvider
  TResult execute<TResult>(ISqlStatement statement) {
    startTimer()
    IDbConnection dbConnection = _dbConnectionProvider
                                      .OpenConnection()
    TResult result = statement.doInConnection(dbConnection)
    dbConnection.close()
    stopTimerAndLog()
    return result
  }
}
\end{lstlisting}

### Sperimentazione {#sperimentazione_transazioni}

La fase di sperimentazione consiste nella scrittura di unit test e test di integrazione. Il team di test lavora su un progetto separato che raggruppa tutti i test relativi al progetto del middleware. Per ogni repository sono stati aggiunti ulteriori unit test a quelli già presenti. Gli integration test aggiunti consistono nel testare il coretto funzionamento delle modifiche apportate con SQLServer e PostgreSQL. In generale, la scrittura di test consiste nello sviluppare una procedura che deve contenere tre fasi:

- *Preparazione*: si dichiarano e si inizializzano tutte le variabili necessarie per l'esecuzione della funzione da testare.
- *Esecuzione*: chiamata della funzione protagonista del test.
- *Asserzione*: verifica dello stato dei dati dopo l'esecuzione della funzione.

Sono state fatte diverse simulazioni nel ambiente di test e sono stati raccolti dati coi tempi di esecuzione delle query prima e dopo la modifica del codice che gestisce le transazioni e l'accesso ai dati. I risultati sono stati soddisfacenti l'applicazione modificata è stata distribuita nel laboratorio Labo Centrale. Dopo un monitoraggio di alcuni giorni si è deciso di intervenire ulteriormente lato software per tentare di eliminare del tutto il blocco del middleware. Inoltre, si sono verificate alcune anomalie che sono correlate alla persistenza di dati ad un alto livello logico.

## Implementazione e utilizzo del algoritmo Redlock {#redlock}

I lock distribuiti sono utili in molti ambienti in cui diversi processi devono operare con risorse condivise in modo mutuamente esclusivo. Redlock è un algoritmo di lock distribuito proposto da Redis [@ref_redlock]. In rete è possibile trovare diverse librerie che implementano l'algoritmo ma il processo aziendale della valutazione dell'utilizzo di una nuova libreria è molto complesso. Quindi si è deciso di implementare e manutenere una propria soluzione. Dopo la fase di sperimentazione descritta nella \+@sec:sperimentazione_transazioni, nel ambiente di produzione si sono verificate alcune anomalie causate dalla rimozione delle transazioni nella maggior parte dei casi. Dopo una fase di analisi era chiaro che non era più possibile utilizzare le transazioni ad un livello logico molto alto. Inserire nuovamente una transazione a quel livello, coinvolgeva centinaia di query e rendeva nullo il lavoro già fato. I casi individuati erano molto chiari e questo capitava quando due middleware processavano in modo concorrente due messaggi di accettazione che logicamente facevano parte di una sola accettazione. Questi casi accadevano di rado è come soluzione si è proposto l'utilizzo di un algoritmo di locking distribuito.

### Implementazione

Per implementare correttamente l'algoritmo è necessario comprendere le proprietà principale del lock:

- *Mutua esclusione*: in un dato momento, solo un client può acquisire i diritti di lock su una risorsa
- *Libero di deadlock*: essere in grado di rilasciare il lock in modo affidabile, il che significa che una risorsa deve essere sbloccata dopo un determinato periodo di tempo.
- *Tolleranza ai guasti*: finché la maggior parte dei nodi Redis sono attivi, i client sono in grado di acquisire e rilasciare i lock.

La prima versione implementata si basa su una singola istanza di Redis e quindi la proprietà *tolleranza ai guasti* non viene rispettata. L'acquisizione di un lock con Redlock inizia con la configurazione di un tempo di blocco predeterminato e un ipotetico tempo di blocco massimo che il processo potrebbe aver bisogno per essere completato. Questi tempi sono rappresentati da `minTimeToLive` e `maxTimeToLive` che sono passati come argomento alla funzione `acquireLock` nel blocco di testo \ref{lst:acquire_lock}. Quindi il lock viene acquisito impostando una chiave con un valore casuale e con un tempo di vita della chiave(riga 9) se la chiave non esiste già. Alla riga 15, la funzione `startAutoExtendTimer` viene invocata su un processo parallelo e controlla se è necessario prolungare il tempo di vita del lock. Se la chiave esiste già vuol dire che la risorsa è stata bloccata da un'altro processo o cliente. Se il lock non è stato acquisito in tempo, non si ottiene alcun lock e al chiamante viene restituito il controllo con il valore impostato a `false`. In caso di errore lato software, il lock viene sbloccato automaticamente allo scadere del tempo di vita della chiave.

\begin{lstlisting}[language = {[Sharp]C}, caption=Funzione di acquisizione di un lock usando l'algoritmo di Redlock,label={lst:acquire_lock}]
bool acquireLock(string key, string lockId, TimeSpan minTimeToLive, TimeSpan maxTimeToLive) {
  var isAcquired = false
  var waitTime = TimeSpan.FromSeconds(30)
  var stopwatch = Stopwatch.StartNew();

  // IC: stopwatch.Elapsed si avvicina sempre a waitTime.Value
  while (!isAcquired && stopwatch.Elapsed <= waitTime.Value) {
    // tentativo dell'acquisizione del lock
    isAcquired = redisClient.SetValueIfNotExists(key, lockId, minTimeToLive);
    if (!isAcquired) {
      wait(100) // aspetta 100 ms
    }
  }

  if (IsAcquired) {
    // assicura la proprietà: Libero di deadlock
    startAutoExtendTimer(key, lockId, maxTimeToLive);
  }

  return isAcquired
}
\end{lstlisting}

\begin{lstlisting}[language = {[Sharp]C}, caption=Esempio di acquisizione e rilascio di un lock, label={lst:acquire_lock_sample}]
var acquired = acquireLock("accettazione NNN", "aaaa-bbbb-cccc-dddd",
                           new TimeSpan(1000), new TimeSpan(30000))
if (acquired) {
  try {
    creaAccettazione(obj)
  } finally () {
    releaseLock("accettazione NNN", "aaaa-bbbb-cccc-dddd")
  }
}
\end{lstlisting}

Per rilasciare il lock, viene utilizzato uno script Lua per verificare se la chiave ha il valore casuale previsto. In tal caso, è possibile eliminare la chiave, in caso contrario non viene modificata perché potrebbe essere un lock creato più recente. Tutti i comandi di uno script Lua vengono eseguiti in sequenza, in una transazione Redis. Quando il lock viene rilasciato, il processo che controlla se è necessario prolungare la vita del lock, viene interrotto.

### Test

Anche per questo sviluppo sono stati aggiunti unit test e test di integrazione. Il tirocinante è stato seguito nella scrittura della documentazione di scenari di test complessi e il loro sviluppo software. L'algoritmo è stato rilasciato in produzione e il suo utilizzo ha risolto i problemi di consistenza presentati nella \+@sec:sperimentazione_transazioni. 

## Code di messaggi {#code_messaggi}

In fase di analisi sono stati raccolti dati sul numero di messaggi che il middleware riceve e processa ogni giorno. La \+@tbl:numero_messaggi mostra riassume la media di esecuzione di ogni tipologia di messaggio. Nel laboratorio Labo Centrale, i messaggi sono configurati per essere ricevuti e processati da istanze del middleware specifiche in base alla categoria dei messaggi. Un'istanza processa solo i messaggi di accettazione, un'altra i messaggi con i risultati provenienti dai dispositivi di analisi in vitro e un altra ancora gestisce i messaggi del PowerExpress. Il limite al numero di istanze è solo quello fisico. Nella \+@sec:sql_server_analisi abbiamo mostrato che aumentare il numero di istanze non risolveva il problema dei blocchi dell'applicazione e che si doveva studiare altri approcci.

-------------------------------------------------------------
Origine o tipologia          N° messaggi    Media di esecuzione 
di messaggio                                di un messaggio
---------------------------- -------------- -----------------
Anagrafica (LIS)              10000           500 ms

Accettazioni (LIS)            15000          4000 ms

Aggiornamenti (LIS)            5000           500 ms

Dispositivi di pre-analitica  50000           200 ms    

PowerExpress (Automazione)   400000           400 ms

Procedure analitiche         200000          2000 ms 

Stockyard                     10000          2000 ms 

Totale                       690000           960 ms
-------------------------------------------------------------

: Numero di messaggi inviati ogni giorno al middleware nel Labo Centrale. {#tbl:numero_messaggi}

Una singola istanza del middleware può gestire 50 messaggi al secondo.

$f(x) = ax^3 + bx^2 + cx + d$ {#eq:my_equation}

### Code di messaggi in entrata

### Code di messaggi in uscita 

### Code di messaggi per il versionamento dei dati

# Conclusioni e sviluppi futuri