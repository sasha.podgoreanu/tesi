\footnotesize
[^*]: Assistenza sanitaria, sanità.
[^1]: Denota un modello concettuale di un sistema. Descrive le varie entità che fanno parte o hanno rilevanza nel sistema e le loro relazioni.
[^2]: Laboratory Information System.
[^3]: Materiale biologico.
[^4]: Una serie di azioni predefinite che devono essere considerate per produrre risultati analitici affidabili e di qualità ben definita. 
[^5]: Con il termine reagenti o reagenti chimici si intendono quelle sostanze che si consumano prendendo parte a una reazione chimica e danno vita ai cosiddetti “prodotti” o “prodotti di reazione”.
[^6]: Total Laboratory Automation Systems.
[^automazione]: Impiego di un insieme di mezzi e procedimenti tecnici che, agendo opportunamente su particolari congegni o dispositivi, assicurano lo svolgimento automatico di un determinato processo.
[^TAT]: Turn around time.
[^campione]: I campioni possono essere di diversi tipi: emocromo, urine, feci, coagulazione.
[^scripting]: Il middleware accetta come linguaggio di scripting C#. Possono essere utilizzati costrutti come if, while, assegnazioni, comparazioni. Viene concesso l'accesso ad alcune variabili globali e ad alcune librerie. 
[^burst]: Rapido aumento del valore di una grandezza variabile nel tempo.
[^routine]: TODO definire routine. 
[^registrazione]: Registrazione della provetta nel laboratorio.
[^DDL]: Data definition language
[^RPC]: Remote Procedure Call
[^DTO]: Data Transfer Object
[^inlet]: L'ingresso di uno strumento di laboratorio dove le provette vengono prelevate per essere processate dallo strumento stesso o per essere instradate verso un altro strumento.
[^mq]: Message Queue, code di messaggi.
[^in-memory]: RAM.