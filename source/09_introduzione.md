\setcounter{page}{1}
\pagenumbering{arabic}
\doublespacing
\setlength{\parindent}{0.5in}



<!--
Nel capitolo @problematiche_middleware blabla bla bla e nel sottocapitolo bla bla @altro_sottocapitolo




Capitolo 1 {#problematiche_middleware}
Sottocapitolo 1 {#altro_sottocapitolo}
-->

# Introduzione

Dall'evoluzione dei laboratori di analisi in vitro, dagli anni 50 con i primi sistemi informatizzati, agli anni 70 con l'integrazione di hardware e di software, è stato possibile iniziare l'automazione in questo settore. Da questa innovazione, alcune attività che prima venivano eseguite manualmente, come il pipettaggio, l’incubazione e le misurazioni, cominciarono ad essere eseguite attraverso i sistemi automatizzati. Queste modifiche hanno reso necessario, negli anni futuri, la creazione di una metodologia per le soluzione automatizzate nei laboratori di analisi.

Il Total Laboratory Automation (TLA), l'argomento che sarà presentato nel \+@sec:problematiche_middleware, costituirà la base per la comprensione del lavoro svolto, che è diventato una metodologia standard all'interno di due laboratori di analisi in vitro, oggetto di ricerca e analisi svolte durante il tirocinio.

Il \+@sec:problematiche_middleware tratterà i concetti chiave del TLA, farà una breve introduzione del'obiettivo di questa ricerca e presenterà un'ipotesi per il problema che è stato identificato. Lo scopo del capitolo sarà quello di identificare i principali ostacoli osservati nell'utilizzo del middleware e dove è possibile intervenire. 

Nel \+@sec:lis saranno presentati gli strumenti di comunicazione del Sistema Informativo di Laboratorio (LIS), partendo dal presupposto che, a causa dell’utilizzo di protocolli diversi da parte delle aziende che producono dispositivi per la TLA, è necessario avare un middleware che gestisce la comunicazione tra questi dispositivi.


Il \+@sec:mma si occuperà di argomenti rilevanti per lo svolgimento del lavoro, con la presentazione di concetti, metodi e analisi. Partendo dal concetto di Test Around Time (TAT), si comprenderà l'importanza della distribuzione dei tempi di risposta per i diversi parametri osservati e come il TAT interferisce nei risultati desiderati. 

Come metodologia, è stata utilizzata la raccolta dati da due laboratori: Labo Centrale e Labo L. Nel Labo Centrale sono state eseguite ricerche, analisi di dati, analisi delle procedure analitiche del laboratorio, monitoraggio della rete interna e altri parametri.

Il laboratorio Labo L processa approssimativamente 2000 campioni al giorno e i blocchi del sistema informativo accadono di rado. Le routine procedono ad una velocità costante e il TAT è considerato accettabile. Al presentare una performance superiore, il TAT del laboratorio Labo L, viene scelto come riferimento per il TAT di Labo Centrale che è da migliorare. Nel laboratorio Labo Centrale, il volume che si accetta è di circa 10000 campioni e 40000 test al giorno. Quando il sistema rallenta, i dispositivi di analisi aspettano ulteriori provette al inlet oppure la programmazione dal middleware per poter eseguire le analisi. 

Nel \+@sec:tu verranno trattate le tecnologie utilizzate per apportare modifiche al progetto in essere e per sviluppare un prototipo software che cerca di risolvere il problema dei blocchi della TLA.


<!-- I termini provetta, boccetta e campione, in questo documento vengono interscambiabili. Anche middleware e sistema informativo del laboratorio. Lo stesso vale per esame analitico, analisi e test.

Nel **[Capitolo 2](#lis)** vengono introdotti i concetti fondamentali del modello di dominio nel ambito di Healthcare, gli attori principali e il flusso di informazioni che il middleware gestisce.
-->

<!-- 

WireLab è un nome inventato che vuoldire middleware e la sua sutite
WireDriver 
WirePrinting
WireLab Front End
WireLab Back End
LabReboot, one of the top products of wLab suite, links to LIS pre-analytical
devices, automation systems and analysers improving process efficiency and
effectiveness in the clinical pathology laboratory
https://www.digitalhealthsummit.it/2021/images/documenti/Lutech_Healthcare.pdf
Servizi Web basati su code di messaggi per migliorare il throughput di un middleware. 

-->