# Problematiche del middleware in un ambiente TLA {#problematiche_middleware}

## Total Laboratory Automation Systems 

Dall'inizio degli anni 1950 i sistemi di diagnostica nei laboratori di analisi si sono sempre evoluti. La prima generazione, i così detti "sistemi", facilitavano la preparazione di reagenti[^5]. La seconda generazione di laboratori di analisi si sviluppano negli anni 1970 dove si possono trovare dispositivi con hardware e software integrato capaci di svolgere alcuni lavori che prima venivano eseguiti manualmente come il pipettaggio, l'incubazione e
le misurazioni. Il Total Laboratory Automation Systems (TLA), la terza generazione di laboratori di analisi [@SASAKI1998217; @FELDER1998257], si evolvono intorno al 1990 quando vengono introdotte le tre procedure del flusso di lavoro: preanalitica, analitica e post-analitica. 

Nella conferenza EuroLabAutomation del 1998 [@doi:10.1177/221106829800300403], Beckman Coulter presenta al suo stand il suo sistema modulare automatizzato di strumenti. I dispositivi principali erano collegati ad un’unica linea di automazione, dall'immissione all’archiviazione dei campioni. Nella stessa conferenza Labotix presenta la sua soluzione TLA che conteneva strumenti collegati fisicamente per la preparazione dei campioni, ordinamento, il trasporto, tracking, storage, cella frigo, gli analizzatori robotizzati capaci di prelevare la provetta dal nastro trasportatore. In entrambe le soluzioni gli strumenti erano gestiti da un software. Nei laboratori moderni Il TLA è diventato una metodologia [@Armbruster2014-ds].

Esistono somiglianze nelle soluzioni proposte per l'automazione, tuttavia è stato sottolineato che sotto la pressione per ridurre i costi, i laboratori clinici hanno cercato risposte nell'utilizzo di moduli di diversi produttori [@10.1093/clinchem/46.5.757].

Un articolo [@HOFFMANN1998203] suggerisce che i TLA devono avere due concetti chiave:

- *Consolidamento*: combinazione di diverse tecnologie o procedure analitiche[^4] su uno strumento o su un gruppo di strumenti collegati.
- *Integrazione*: collegamento di strumenti analitici o gruppi di strumenti con dispositivi pre e post analitici.

In un TLA, per applicare le proprietà descritte, deve esistere una componente che si infrappone tra dispositivi di diversi produttori e facilita la comunicazione.

## Middleware: collo di bottiglia per il TLA {#collo_di_bottiglia_TLA}

Una soluzione per gestire la comunicazione tra più tecnologie e moduli di strumenti è di utilizzare un middleware. Il termine middleware [@10.1145/374308.374365] si riferisce ad una componente che consente la connessione modulare di software distribuito e facilita il loro interfacciamento. Il laboratorio valutato nel presente studio è un laboratorio centrale ad alto volume che accetta circa 10000 campioni ed esegue 40000 test al giorno. Chiameremo questo laboratorio Labo Centrale. I campioni in provette provenienti da cliniche e ospedali periferici vengono inviati a Labo Centrale. Il laboratorio implementa le metodologie finora presentate, cioè TLA, sistemi da diversi produttori e il middleware che viene fornito dall'azienda che ha richiesto lo studio.

Nell'utilizzo di uno strato di comunicazione intermediario nascono molte problematiche. Il turn around time (TAT) è spesso utilizzato dai medici come indicatore delle prestazioni di un laboratorio [@Holman2002-gt]. L'indicatore TAT sarà approfondito nel capitolo successivo.

Il middleware permette ai tecnici di laboratorio di definire regole di validazione per tipo di campione[^campione] in un linguaggio di scripting[^scripting]. I tecnici di laboratorio hanno conoscenze minime della teoria degli algoritmi, e le regole sono da considerarsi segreto aziendale. Le regole di validazione sono eseguite ad ogni messaggio ricevuto dal middleware in base ad una configurazione. Quando sono stati rilevati rallentamenti nell'esecuzione delle regole, l'azienda che fornisce il middleware non ha avuto la possibilità di intervenire per ottimizzarle secondo le giustificative fornite sopra.

Un piccolo rallentamento di ogni messaggio significa un TAT maggiore che ha un impatto negativo su tutta la catena dell'automazione e sulle prestazioni del laboratorio. Nelle ore di punta delle routine i dispositivi si scambiano una quantità elevata di messaggi con il middleware e l'elaborazione rallenta significativamente tutto il sistema fino al blocco totale. Un altro problema è il burst di messaggi che il middleware riceve. Il middleware poteva processare al massimo 2000 campioni in un'ora. Al raggiungimento di tale limite, i messaggi venivano scartati forzando i dispositivi, che rimanevano bloccati nel tentativo di rinviare i messaggi non trasmessi. Come conseguenza, le provette con il materiale biologico venivano inviate all’armadio refrigerato che a causa del blocco del middleware non funzionava correttamente, non accettando le provette all’ingresso. Le corsie del PowerExpress, automazione che trasporta le provette alle procedure analitiche, erano completamente congestionate. Gli operatori dovevano fare a meno dell'automazione e caricare le provette manualmente annullando l'utilità dei dispositivi tecnologici avanzati collegati all’impianto. Il middleware era diventato un collo di bottiglia per tutta l'automazione.

<!--PROF: se ho capito bene, l’obiettivo è stato quello di identificare i colli di bottiglia di una soluzione già 
esistente e migliorarla
dovrà anche fare capire cosa ha fatto lei personalmente e cosa in team -->

## Obiettivo

L'obiettivo principale che sarà presentato in questo documento è di trovare un'architettura per la gestione di messaggi del middleware che deve essere scalabile e modulabile. In base al volume di lavoro del laboratorio, deve essere possibile fare il deployment di istanze multiple del middleware per garantire un TAT accettabile.

## Ipotesi

L'ipotesi di una breve analisi dei blocchi costanti giornalieri del TLA è un sovraccarico di dati in ingresso che il middleware deve gestire. L'architettura del middleware è gia scalabile e permette di fare il deployment di istanze multiple del middleware. Ma il SQL Server è il principale data base supportato dal middleware e permette la configurazione di una singola istanza. Un uso improprio delle risorse del database potrebbe essere la causa degli rallentamenti.