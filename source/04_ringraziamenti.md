# Ringraziamenti {.unnumbered}

Vorrei ringraziare alcune persone che mi hanno dato supporto e aiuto durante l’elaborazione di questo progetto. Un posto speciale nei miei ringraziamenti è destinato a Gisiani per il continuo appoggio e sostegno datomi in questi
anni. 

Vorrei ringraziare la mia mamma per il suo sostegno e l'incentivo allo studio.

Vorrei ringraziare il mio relatore, il professore Luca Anselma, per l’opportunità datami e, soprattutto, per la sua disponibilità e pazienza durante il periodo della stesura della tesi.

\newpage



