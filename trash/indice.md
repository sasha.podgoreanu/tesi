# Introduzione
# Progetto formativo

# Il Sistema Informativo di Laboratorio (LIS)
## Le funzionalità del LIS
### Campioni (proveta) con sangue urine, escremento
### Richiesta di un paziente
## Strumenti di analisi in vitro
### cosa fanno e come, velocita, limiti.
## Utilizzo dei middleware nei laboratori
### pro/contra

+ strumenti memmoria limitata
+ velocita bassa per analizzare campioni
+ LIS a una certa ora (11) inizia ad inviare richieste/messaggi al middleware e questo non riesce a processare tutto e perde alcuni messaggi. in base al overload dei messaggi 

# Refactoring della architettura del middleware
## Architettura Pre refactoring
## Ricerca e analisi documentale
## Teorie e algoritmi
## Architettura post refactoring

# Tecnologie utilizzate
## ServiceStack
## Redis
## JWT 

# Il metodo svilupato
## Code di messagi 

# Sperimentazione
## Unit tests
## Stress tests

# Conclusioni e sviluppi futuri 


Code di messagi persistenti con Redis