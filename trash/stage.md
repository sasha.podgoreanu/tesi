## Obiettivi e modalità del tirocinio:

Gestione dei flussi applicativi con  `ServiceStack` Message-based WebSevices su un sistema informativo già in essere sviluppato in C#, basato su framework ServiceStack, multi-database `SQL Serve`r e `PostgreSQL`, databse documentale `MongoDB` e database in-memory `Redis`. Esperienza operativa nell'ambito dei linguaggi di programmazione C#, JSON, `OrmLite`, Sql su database SQL Server e PostgreSQL e nel design pattern architetturale Modello-Vista-Controllore. Il tirocinante seguirà le fasi di analisi, sviluppo e collaudo di uno o più moduli che si occuperanno di comunicare con la strumentazione di laboratorio. Si partirà con l'affiancamento al gruppo di analisi per la raccolta formale dei requisiti e la produzione di documentazione tecnico-funzionale, successivamente il tirocinante seguirà operativamente gli sviluppi. Terminata la fase di sviluppo il tirocinante affiancherà il gruppo di collaudo nel testing e nella realizzazione della documentazione.  

## Attività del tirocinante:

Lo stagista sarà inserito in un gruppo di quattro sviluppatori e gli sarà presentato un quadro generale dell’intero flusso di lavoro dell’applicazione.

## Attività principali:

+ Analizzare i problemi di performance attuali dell’applicativo che riguardano l’acquisizione di risultati dagli strumenti di analisi in vitro (IVD). Individuato il problema lo stagista dovrà apportare le modifiche necessarie per il miglioramento del sistema;

+ Modernizzare la struttura del modulo “Driver” che si occupa degli interfacciamenti strumentali di laboratorio;

+ Creare un nuovo progetto per inserire gli unit test e i test di integrazione al modulo “Driver” per l'attività di testing;

+ Risolvere un problema riguardante il sovraccarico di messaggi http generato dai driver strumentali durante la gestione e la scrittura dei log e dei monitor;

+ Sviluppare dei moduli applicativi per due nuovi strumenti utilizzando le nuove funzionalità riviste.




## Email da prof
Buongiorno,
ho segnalato alla commissione stage che verbalizzerà lo stage.
Sono il relatore. Inizio a darle qualche indicazione sulla relazione (tesi):
- trova il frontespizio qui http://laurea.educ.di.unito.it/index.php/accreditamento/consultazione/ListaDocumenti?commissione=9
- nella seconda o terza pagina deve includere una dichiarazione di originalità che trova nel regolamento della prova finale
- non conosco indicazioni ufficiali sulla formattazione del resto della tesi ma riassumo qui ciò che viene di solito usato:
* Margini: 3 cm superiore, 2,5 cm destra sinistra e inferiore, 1 cm per la rilegatura (quindi a sinistra un totale di 3,5cm).
* Carattere: Times New Roman, Arial o Calibri 12 punti, con interlinea 1.5, giustificato.
* I titoli devono essere ovviamente più grandi del testo (ma i titoli dei capitoli li metterei più grandi di 16)
* Figure e tabelle devono essere numerate, avere una didascalia ed essere citate nel testo

Inizi scrivendo l'indice, che servirà per dare "struttura" alla relazione. Nell'indice probabilmente ci sarà una parte in cui illustrerà le tecnologie e gli strumenti usati e poi una parte, importante, in cui illustrerà il lavoro che ha fatto. Il primo capitolo sarà l'introduzione e l'ultimo capitolo le conclusioni. Questi due (introduzione e conclusioni) dovrà scriverli per ultimi perché solo alla fine sarà in grado di introdurre propriamente la relazione.
Il primo passo quindi è la scrittura dell'indice, che mi manderà e, dopo che l'ho rivisto, potrà andare avanti.